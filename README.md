# Eurorack module tester.

This is a fork of [https://github.com/pichenettes/module_tester](https://github.com/pichenettes/module_tester). The original old-format Eagle design files have been replaced by new-format versions, and KiCad format versions have been added. There also is a folder containing the slightly modified version I built.

Original README.md follows.

----

This project is a test signal generator for Eurorack synth modules. It also provides frequency/clock measurement tools, and a +/12V and +5V power supply - making it a compact "all in one" box for powering and debugging a module.

Original developer: Emilie Gillet (emilie.o.gillet@gmail.com)

Forked from pichenettes' version and identical except:

- Eagle brd and sch files converted to XML (making possible import into KiCad)
- KiCad import added (```module_tester/hardware_design_pcb/KiCad```)
- Modified version added (```module_tester/hardware_design_pcb/KiCad-mod```)

Changes in the modified version:

- PCB size increased to 213x133 mm (to match dimensions of a cigar box I intended to use as an enclosure).
- Footprints modified to show reference and (where applicable) value for all components on silkscreen.
- Footprints for MIDI and 1/4" audio jacks changed to match parts I had. (MIDI from Amazon and labeled "Bastex Right Angle Female", apparently same footprint as Kycon KCDX-5S-N, Digi-Key SKU 2092-KCDX-5S-N-ND; audio jack from Tayda, SKU A-1122.)
- Eurorack style power header footprint changed from unshrouded vertical to shrouded right angle; **see Errata below**.
- Mounting holes for LCD added, to be used to secure the LCD in case one wants to socket this.
- A few other purely cosmetic changes.

**Errata:** 

- On the first run PCB, the 16 pin power header **is oriented incorrectly**. This is because I hadn't noticed that in the schematic, -12 V was connected to pins 15–16 instead of 1–2. -12 V is at the end closest to the diodes and switches. A right angle shrouded header mounted under the PCB **will force reversed power voltages** resulting in damage to connected module. Instead use: right angle shrouded header mounted on top of PCB *or* vertical shrouded header under PCB with orientation notch toward edge of PCB *or* unshrouded header. Double check header orientation and voltage connections before applying power!

- On the first run PCB, the negative terminal of C26 is floating. It should be connected to ground. Run a wire to the middle terminal of IC8.

The firmware is released under a GPL3.0 license. The PCB layouts and schematics are released under a Creative Commons cc-by-sa 3.0 license.

To build code and flash a blank 644p:
make -f module_tester/makefile bootstrap
