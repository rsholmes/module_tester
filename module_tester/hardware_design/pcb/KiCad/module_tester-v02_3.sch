EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 12505 9687
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5900 4200 5800 4200
Wire Wire Line
	5800 4200 5800 4300
Text GLabel 5900 4200 0    10   BiDi ~ 0
GND
Wire Wire Line
	3000 7000 3000 7100
Wire Wire Line
	3000 7100 3000 7200
Wire Wire Line
	3000 7100 3400 7100
Wire Wire Line
	3400 7100 3400 7200
Connection ~ 3000 7100
Text GLabel 3000 7000 0    10   BiDi ~ 0
GND
Wire Wire Line
	7100 4300 7100 4400
Wire Wire Line
	7200 4300 7100 4300
Wire Wire Line
	7100 4200 7100 4300
Wire Wire Line
	7100 4200 7200 4200
Connection ~ 7100 4300
Text GLabel 7100 4300 0    10   BiDi ~ 0
GND
Wire Wire Line
	4900 4200 5000 4200
Wire Wire Line
	5000 4200 5000 4300
Text GLabel 4900 4200 0    10   BiDi ~ 0
GND
Wire Wire Line
	9300 2100 9200 2100
Wire Wire Line
	9200 2100 9200 2200
Text GLabel 9300 2100 0    10   BiDi ~ 0
GND
Wire Wire Line
	1800 1800 1700 1800
Wire Wire Line
	1700 1800 1700 1900
Text GLabel 1800 1800 0    10   BiDi ~ 0
GND
Wire Wire Line
	2400 2300 2400 2400
Text GLabel 2400 2300 0    10   BiDi ~ 0
GND
Wire Wire Line
	10600 2200 10500 2200
Wire Wire Line
	10500 2200 10500 2300
Text GLabel 10600 2200 0    10   BiDi ~ 0
GND
Wire Wire Line
	5900 2100 5800 2100
Wire Wire Line
	5800 2100 5800 2200
Text GLabel 5900 2100 0    10   BiDi ~ 0
GND
Wire Wire Line
	7200 2200 7100 2200
Wire Wire Line
	7100 2200 7100 2300
Text GLabel 7200 2200 0    10   BiDi ~ 0
GND
Wire Wire Line
	5900 5200 5800 5200
Wire Wire Line
	5800 5200 5800 5300
Text GLabel 5900 5200 0    10   BiDi ~ 0
GND
Wire Wire Line
	5200 7000 5100 7000
Wire Wire Line
	5100 7000 5100 7100
Text GLabel 5200 7000 0    10   BiDi ~ 0
GND
Wire Wire Line
	5700 7400 5700 7500
Text GLabel 5700 7400 0    10   BiDi ~ 0
GND
Wire Wire Line
	2600 7400 2600 7500
Wire Wire Line
	2600 7500 2600 7600
Wire Wire Line
	2300 7300 2300 7500
Wire Wire Line
	2300 7500 2600 7500
Wire Wire Line
	2000 7300 2000 7500
Wire Wire Line
	2000 7500 2300 7500
Connection ~ 2600 7500
Connection ~ 2300 7500
Text GLabel 2600 7400 0    10   BiDi ~ 0
GND
Wire Wire Line
	9400 4000 9300 4000
Wire Wire Line
	9300 4000 9300 4100
Text GLabel 9400 4000 0    10   BiDi ~ 0
GND
Wire Wire Line
	6600 4100 6500 4100
Wire Wire Line
	2800 7400 2800 7600
Wire Wire Line
	2800 7600 3000 7600
Wire Wire Line
	3000 7600 3000 7700
Wire Wire Line
	3000 7500 3000 7600
Connection ~ 3000 7600
Text GLabel 2800 7400 0    10   BiDi ~ 0
VEE
Wire Wire Line
	2800 6800 2800 6600
Wire Wire Line
	2800 6600 3000 6600
Wire Wire Line
	3000 6700 3000 6600
Wire Wire Line
	3000 6600 3000 6500
Connection ~ 3000 6600
Text GLabel 2800 6800 0    10   BiDi ~ 0
VCC
Wire Wire Line
	5800 3700 6300 3700
Wire Wire Line
	6400 3400 5800 3400
Wire Wire Line
	5800 3400 5800 3700
Wire Wire Line
	5700 4000 5800 4000
Wire Wire Line
	5800 4000 5900 4000
Wire Wire Line
	5800 3700 5800 4000
Connection ~ 5800 4000
Connection ~ 5800 3700
Wire Wire Line
	6700 3400 7100 3400
Wire Wire Line
	7100 4100 7200 4100
Wire Wire Line
	7000 4100 7100 4100
Wire Wire Line
	6700 3700 7100 3700
Wire Wire Line
	7100 3700 7100 4100
Wire Wire Line
	7100 3400 7100 3700
Connection ~ 7100 4100
Connection ~ 7100 3700
Wire Wire Line
	5300 4000 5200 4000
Wire Wire Line
	5200 4000 4900 4000
Wire Wire Line
	5300 5000 5200 5000
Wire Wire Line
	5200 5000 5200 4000
Wire Wire Line
	5200 5000 5200 5800
Connection ~ 5200 4000
Connection ~ 5200 5000
Wire Wire Line
	1800 1700 1700 1700
Text GLabel 1700 1700 0    70   BiDi ~ 0
MOSI
Wire Wire Line
	1800 1600 1700 1600
Text GLabel 1700 1600 0    70   BiDi ~ 0
SCK
Wire Wire Line
	1800 1500 1700 1500
Text GLabel 1700 1500 0    70   BiDi ~ 0
SS
Wire Wire Line
	3000 1500 3100 1500
Text Label 3100 1500 0    70   ~ 0
DAC_TONE
Wire Wire Line
	8300 1900 8200 1900
Text Label 8200 1900 2    70   ~ 0
DAC_TONE
Wire Wire Line
	3000 1600 3100 1600
Text Label 3100 1600 0    70   ~ 0
DAC_CV
Wire Wire Line
	5000 1100 5000 1000
Wire Wire Line
	5000 1000 4700 1000
Wire Wire Line
	4700 1000 4700 1300
Wire Wire Line
	4700 1300 4800 1300
Wire Wire Line
	4700 1300 4600 1300
Connection ~ 4700 1300
Text Label 4600 1300 2    70   ~ 0
DAC_CV
Wire Wire Line
	8600 1900 8700 1900
Wire Wire Line
	9100 1900 9200 1900
Wire Wire Line
	9200 1900 9300 1900
Wire Wire Line
	9400 1500 9200 1500
Wire Wire Line
	9200 1500 9200 1900
Wire Wire Line
	9200 1200 9200 1500
Wire Wire Line
	9500 1200 9200 1200
Connection ~ 9200 1900
Connection ~ 9200 1500
Wire Wire Line
	9800 1500 10500 1500
Wire Wire Line
	10500 1500 10500 2000
Wire Wire Line
	10500 2000 10600 2000
Wire Wire Line
	9800 1200 10500 1200
Wire Wire Line
	10500 1200 10500 1500
Wire Wire Line
	10400 2000 10500 2000
Connection ~ 10500 1500
Connection ~ 10500 2000
Wire Wire Line
	5900 1900 5800 1900
Wire Wire Line
	6000 1200 5800 1200
Wire Wire Line
	5800 1200 5800 1300
Wire Wire Line
	5800 1300 5800 1600
Wire Wire Line
	5800 1600 5800 1900
Wire Wire Line
	5700 1300 5800 1300
Wire Wire Line
	5700 1900 5800 1900
Wire Wire Line
	6000 1600 5800 1600
Connection ~ 5800 1300
Connection ~ 5800 1900
Connection ~ 5800 1600
Wire Wire Line
	6400 1200 7100 1200
Wire Wire Line
	7100 1200 7100 1600
Wire Wire Line
	7100 1600 7100 2000
Wire Wire Line
	7100 2000 7200 2000
Wire Wire Line
	6300 1600 7100 1600
Wire Wire Line
	7000 2000 7100 2000
Connection ~ 7100 1600
Connection ~ 7100 2000
Wire Wire Line
	6100 4700 5800 4700
Wire Wire Line
	5800 4700 5800 5000
Wire Wire Line
	5800 5000 5700 5000
Wire Wire Line
	5800 5000 5900 5000
Connection ~ 5800 5000
Wire Wire Line
	6500 4700 6600 4700
Wire Wire Line
	6600 4700 6600 5100
Wire Wire Line
	6600 5100 6500 5100
Wire Wire Line
	6600 5100 6700 5100
Connection ~ 6600 5100
Text GLabel 6700 5100 2    70   BiDi ~ 0
PITCH_DETECTOR
Wire Wire Line
	5900 6700 5700 6700
Wire Wire Line
	5700 6700 5700 7000
Wire Wire Line
	5700 7000 5600 7000
Wire Wire Line
	5700 7000 5800 7000
Wire Wire Line
	5700 7000 5700 7100
Connection ~ 5700 7000
Wire Wire Line
	6200 7000 6400 7000
Text GLabel 6200 7000 0    10   BiDi ~ 0
+5V
Wire Wire Line
	2600 6600 2600 6700
Wire Wire Line
	2600 6700 2600 6800
Wire Wire Line
	2600 6700 2300 6700
Wire Wire Line
	2300 6700 2300 7000
Wire Wire Line
	2300 6700 2000 6700
Wire Wire Line
	2000 7000 2000 6700
Connection ~ 2600 6700
Connection ~ 2300 6700
Text GLabel 2600 6600 0    10   BiDi ~ 0
+5V
Wire Wire Line
	2400 800  2400 900 
Text GLabel 2400 800  0    10   BiDi ~ 0
+5V
Wire Wire Line
	5200 6100 5200 6500
Wire Wire Line
	5200 6500 5300 6500
Wire Wire Line
	6000 6100 5800 6100
Wire Wire Line
	5800 6100 5800 6500
Wire Wire Line
	5800 6500 5700 6500
Wire Wire Line
	5800 6500 5900 6500
Wire Wire Line
	6000 5800 5800 5800
Wire Wire Line
	5800 5800 5800 6100
Connection ~ 5800 6500
Connection ~ 5800 6100
Wire Wire Line
	6400 6100 6600 6100
Wire Wire Line
	6600 6100 6600 6600
Wire Wire Line
	6500 6600 6600 6600
Wire Wire Line
	6600 6600 6700 6600
Wire Wire Line
	6300 5800 6600 5800
Wire Wire Line
	6600 5800 6600 6100
Connection ~ 6600 6600
Connection ~ 6600 6100
Text GLabel 6700 6600 2    70   BiDi ~ 0
ADC
Wire Wire Line
	4700 1900 4800 1900
Wire Wire Line
	5200 1900 5300 1900
Wire Wire Line
	4500 1700 4500 1600
Wire Wire Line
	4500 1600 4200 1600
Wire Wire Line
	4200 1600 4200 1900
Wire Wire Line
	4200 1900 4300 1900
Wire Wire Line
	4100 1900 4200 1900
Connection ~ 4200 1900
Text GLabel 4100 1900 0    70   BiDi ~ 0
REF_-4.096
Wire Wire Line
	9400 3800 9300 3800
Wire Wire Line
	9300 3800 9300 3600
Wire Wire Line
	9300 3600 10100 3600
Wire Wire Line
	10100 3600 10100 3900
Wire Wire Line
	10100 3900 10000 3900
Wire Wire Line
	5200 1300 5300 1300
Wire Wire Line
	6500 2000 6600 2000
Wire Wire Line
	9900 2000 10000 2000
$Comp
L module_tester-v02-eagle-import:MONOSWSLIM J8
U 1 1 5B92CF69
P 7700 4200
AR Path="/5B92CF69" Ref="J8"  Part="1" 
AR Path="/61BE22D1/5B92CF69" Ref="J8"  Part="1" 
F 0 "J8" V 7550 4000 59  0000 L BNN
F 1 "MONOSWSLIM" H 7700 4200 50  0001 C CNN
F 2 "module_tester-v02:NRJ4HF" H 7700 4200 50  0001 C CNN
F 3 "" H 7700 4200 50  0001 C CNN
	1    7700 4200
	-1   0    0    1   
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND44
U 1 1 1165F8F0
P 7100 4500
AR Path="/1165F8F0" Ref="#GND44"  Part="1" 
AR Path="/61BE22D1/1165F8F0" Ref="#GND044"  Part="1" 
F 0 "#GND044" H 7100 4500 50  0001 C CNN
F 1 "GND" H 7000 4400 59  0000 L BNN
F 2 "" H 7100 4500 50  0001 C CNN
F 3 "" H 7100 4500 50  0001 C CNN
	1    7100 4500
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND54
U 1 1 F28BF559
P 5800 4400
AR Path="/F28BF559" Ref="#GND54"  Part="1" 
AR Path="/61BE22D1/F28BF559" Ref="#GND054"  Part="1" 
F 0 "#GND054" H 5800 4400 50  0001 C CNN
F 1 "GND" H 5700 4300 59  0000 L BNN
F 2 "" H 5800 4400 50  0001 C CNN
F 3 "" H 5800 4400 50  0001 C CNN
	1    5800 4400
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:C-US025-025X050 C4
U 1 1 284EA6E9
P 3000 6800
AR Path="/284EA6E9" Ref="C4"  Part="1" 
AR Path="/61BE22D1/284EA6E9" Ref="C4"  Part="1" 
F 0 "C4" H 3040 6825 59  0000 L BNN
F 1 "100n" H 3040 6635 59  0000 L BNN
F 2 "module_tester-v02:C025-025X050" H 3000 6800 50  0001 C CNN
F 3 "" H 3000 6800 50  0001 C CNN
	1    3000 6800
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:LETTER_L #FRAME3
U 2 1 F8E7202B
P 7700 8800
AR Path="/F8E7202B" Ref="#FRAME3"  Part="2" 
AR Path="/61BE22D1/F8E7202B" Ref="#FRAME3"  Part="2" 
F 0 "#FRAME3" H 7700 8800 50  0001 C CNN
F 1 "LETTER_L" H 7700 8800 50  0001 C CNN
F 2 "" H 7700 8800 50  0001 C CNN
F 3 "" H 7700 8800 50  0001 C CNN
	2    7700 8800
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:FRAME_A_L #FRAME9
U 1 1 41B3B3D2
P 900 8800
AR Path="/41B3B3D2" Ref="#FRAME9"  Part="1" 
AR Path="/61BE22D1/41B3B3D2" Ref="#FRAME9"  Part="1" 
F 0 "#FRAME9" H 900 8800 50  0001 C CNN
F 1 "FRAME_A_L" H 900 8800 50  0001 C CNN
F 2 "" H 900 8800 50  0001 C CNN
F 3 "" H 900 8800 50  0001 C CNN
	1    900  8800
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:FRAME_A_L #FRAME9
U 2 1 41B3B3DE
P 7700 8800
AR Path="/41B3B3DE" Ref="#FRAME9"  Part="2" 
AR Path="/61BE22D1/41B3B3DE" Ref="#FRAME9"  Part="2" 
F 0 "#FRAME9" H 7700 8800 50  0001 C CNN
F 1 "FRAME_A_L" H 7700 8800 50  0001 C CNN
F 2 "" H 7700 8800 50  0001 C CNN
F 3 "" H 7700 8800 50  0001 C CNN
	2    7700 8800
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:C-US025-025X050 C5
U 1 1 F9108C4C
P 3000 7300
AR Path="/F9108C4C" Ref="C5"  Part="1" 
AR Path="/61BE22D1/F9108C4C" Ref="C5"  Part="1" 
F 0 "C5" H 3040 7325 59  0000 L BNN
F 1 "100n" H 3040 7135 59  0000 L BNN
F 2 "module_tester-v02:C025-025X050" H 3000 7300 50  0001 C CNN
F 3 "" H 3000 7300 50  0001 C CNN
	1    3000 7300
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND42
U 1 1 C4E41BBE
P 3400 7300
AR Path="/C4E41BBE" Ref="#GND42"  Part="1" 
AR Path="/61BE22D1/C4E41BBE" Ref="#GND042"  Part="1" 
F 0 "#GND042" H 3400 7300 50  0001 C CNN
F 1 "GND" H 3300 7200 59  0000 L BNN
F 2 "" H 3400 7300 50  0001 C CNN
F 3 "" H 3400 7300 50  0001 C CNN
	1    3400 7300
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:VEE #SUPPLY2
U 1 1 33EFDB7C
P 3000 7800
AR Path="/33EFDB7C" Ref="#SUPPLY2"  Part="1" 
AR Path="/61BE22D1/33EFDB7C" Ref="#SUPPLY02"  Part="1" 
F 0 "#SUPPLY02" H 3000 7800 50  0001 C CNN
F 1 "VEE" H 2925 7925 59  0000 L BNN
F 2 "" H 3000 7800 50  0001 C CNN
F 3 "" H 3000 7800 50  0001 C CNN
	1    3000 7800
	-1   0    0    1   
$EndComp
$Comp
L module_tester-v02-eagle-import:VCC #P+28
U 1 1 E6EDA88F
P 3000 6500
AR Path="/E6EDA88F" Ref="#P+28"  Part="1" 
AR Path="/61BE22D1/E6EDA88F" Ref="#P+028"  Part="1" 
F 0 "#P+028" H 3000 6500 50  0001 C CNN
F 1 "VCC" H 2960 6640 59  0000 L BNN
F 2 "" H 3000 6500 50  0001 C CNN
F 3 "" H 3000 6500 50  0001 C CNN
	1    3000 6500
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:R-US_0204_7 R1
U 1 1 1C1E0795
P 6800 4100
AR Path="/1C1E0795" Ref="R1"  Part="1" 
AR Path="/61BE22D1/1C1E0795" Ref="R1"  Part="1" 
F 0 "R1" H 6650 4159 59  0000 L BNN
F 1 "470" H 6650 3970 59  0000 L BNN
F 2 "module_tester-v02:0204_7" H 6800 4100 50  0001 C CNN
F 3 "" H 6800 4100 50  0001 C CNN
	1    6800 4100
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:R-US_0204_7 R4
U 1 1 4F739D54
P 6500 3700
AR Path="/4F739D54" Ref="R4"  Part="1" 
AR Path="/61BE22D1/4F739D54" Ref="R4"  Part="1" 
F 0 "R4" H 6350 3759 59  0000 L BNN
F 1 "33k" H 6350 3570 59  0000 L BNN
F 2 "module_tester-v02:0204_7" H 6500 3700 50  0001 C CNN
F 3 "" H 6500 3700 50  0001 C CNN
	1    6500 3700
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:C-US025-025X050 C1
U 1 1 569CF812
P 6500 3400
AR Path="/569CF812" Ref="C1"  Part="1" 
AR Path="/61BE22D1/569CF812" Ref="C1"  Part="1" 
F 0 "C1" H 6540 3425 59  0000 L BNN
F 1 "18p" H 6540 3235 59  0000 L BNN
F 2 "module_tester-v02:C025-025X050" H 6500 3400 50  0001 C CNN
F 3 "" H 6500 3400 50  0001 C CNN
	1    6500 3400
	0    -1   -1   0   
$EndComp
$Comp
L module_tester-v02-eagle-import:R-US_0204_7 R7
U 1 1 F9EF3F38
P 5500 4000
AR Path="/F9EF3F38" Ref="R7"  Part="1" 
AR Path="/61BE22D1/F9EF3F38" Ref="R7"  Part="1" 
F 0 "R7" H 5350 4059 59  0000 L BNN
F 1 "100k" H 5350 3870 59  0000 L BNN
F 2 "module_tester-v02:0204_7" H 5500 4000 50  0001 C CNN
F 3 "" H 5500 4000 50  0001 C CNN
	1    5500 4000
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND2
U 1 1 13A376EC
P 5000 4400
AR Path="/13A376EC" Ref="#GND2"  Part="1" 
AR Path="/61BE22D1/13A376EC" Ref="#GND02"  Part="1" 
F 0 "#GND02" H 5000 4400 50  0001 C CNN
F 1 "GND" H 4900 4300 59  0000 L BNN
F 2 "" H 5000 4400 50  0001 C CNN
F 3 "" H 5000 4400 50  0001 C CNN
	1    5000 4400
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND4
U 1 1 D896219B
P 9200 2300
AR Path="/D896219B" Ref="#GND4"  Part="1" 
AR Path="/61BE22D1/D896219B" Ref="#GND04"  Part="1" 
F 0 "#GND04" H 9200 2300 50  0001 C CNN
F 1 "GND" H 9100 2200 59  0000 L BNN
F 2 "" H 9200 2300 50  0001 C CNN
F 3 "" H 9200 2300 50  0001 C CNN
	1    9200 2300
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND7
U 1 1 9CDE76EF
P 1700 2000
AR Path="/9CDE76EF" Ref="#GND7"  Part="1" 
AR Path="/61BE22D1/9CDE76EF" Ref="#GND07"  Part="1" 
F 0 "#GND07" H 1700 2000 50  0001 C CNN
F 1 "GND" H 1600 1900 59  0000 L BNN
F 2 "" H 1700 2000 50  0001 C CNN
F 3 "" H 1700 2000 50  0001 C CNN
	1    1700 2000
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:MCP4822 IC3
U 1 1 5ED25CA7
P 2400 1600
AR Path="/5ED25CA7" Ref="IC3"  Part="1" 
AR Path="/61BE22D1/5ED25CA7" Ref="IC3"  Part="1" 
F 0 "IC3" H 2300 2200 56  0000 R TNN
F 1 "MCP4822" H 2400 1600 50  0001 C CNN
F 2 "module_tester-v02:DIL08" H 2400 1600 50  0001 C CNN
F 3 "" H 2400 1600 50  0001 C CNN
	1    2400 1600
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND8
U 1 1 DF24F9E8
P 2400 2500
AR Path="/DF24F9E8" Ref="#GND8"  Part="1" 
AR Path="/61BE22D1/DF24F9E8" Ref="#GND08"  Part="1" 
F 0 "#GND08" H 2400 2500 50  0001 C CNN
F 1 "GND" H 2300 2400 59  0000 L BNN
F 2 "" H 2400 2500 50  0001 C CNN
F 3 "" H 2400 2500 50  0001 C CNN
	1    2400 2500
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:CPOL-USE2.5-6 C6
U 1 1 DCD4E3E0
P 8400 1900
AR Path="/DCD4E3E0" Ref="C6"  Part="1" 
AR Path="/61BE22D1/DCD4E3E0" Ref="C6"  Part="1" 
F 0 "C6" H 8440 1925 59  0000 L BNN
F 1 "4.7u" H 8440 1735 59  0000 L BNN
F 2 "module_tester-v02:E2,5-6" H 8400 1900 50  0001 C CNN
F 3 "" H 8400 1900 50  0001 C CNN
	1    8400 1900
	0    -1   -1   0   
$EndComp
$Comp
L module_tester-v02-eagle-import:R-US_0204_7 R11
U 1 1 2A0DDB7E
P 8900 1900
AR Path="/2A0DDB7E" Ref="R11"  Part="1" 
AR Path="/61BE22D1/2A0DDB7E" Ref="R11"  Part="1" 
F 0 "R11" H 8750 1959 59  0000 L BNN
F 1 "62k" H 8750 1770 59  0000 L BNN
F 2 "module_tester-v02:0204_7" H 8900 1900 50  0001 C CNN
F 3 "" H 8900 1900 50  0001 C CNN
	1    8900 1900
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:R-US_0204_7 R15
U 1 1 8FBABD33
P 9600 1500
AR Path="/8FBABD33" Ref="R15"  Part="1" 
AR Path="/61BE22D1/8FBABD33" Ref="R15"  Part="1" 
F 0 "R15" H 9450 1559 59  0000 L BNN
F 1 "150k" H 9450 1370 59  0000 L BNN
F 2 "module_tester-v02:0204_7" H 9600 1500 50  0001 C CNN
F 3 "" H 9600 1500 50  0001 C CNN
	1    9600 1500
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:C-US025-025X050 C7
U 1 1 CEF095AF
P 9600 1200
AR Path="/CEF095AF" Ref="C7"  Part="1" 
AR Path="/61BE22D1/CEF095AF" Ref="C7"  Part="1" 
F 0 "C7" H 9640 1225 59  0000 L BNN
F 1 "18p" H 9640 1035 59  0000 L BNN
F 2 "module_tester-v02:C025-025X050" H 9600 1200 50  0001 C CNN
F 3 "" H 9600 1200 50  0001 C CNN
	1    9600 1200
	0    -1   -1   0   
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND19
U 1 1 48B3EBD8
P 10500 2400
AR Path="/48B3EBD8" Ref="#GND19"  Part="1" 
AR Path="/61BE22D1/48B3EBD8" Ref="#GND019"  Part="1" 
F 0 "#GND019" H 10500 2400 50  0001 C CNN
F 1 "GND" H 10400 2300 59  0000 L BNN
F 2 "" H 10500 2400 50  0001 C CNN
F 3 "" H 10500 2400 50  0001 C CNN
	1    10500 2400
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:TL074P IC2
U 1 1 541775CA
P 6200 4100
AR Path="/541775CA" Ref="IC2"  Part="1" 
AR Path="/61BE22D1/541775CA" Ref="IC2"  Part="1" 
F 0 "IC2" H 6300 4225 59  0001 L BNN
F 1 "TL074P" H 6300 3900 59  0001 L BNN
F 2 "module_tester-v02:DIL14" H 6200 4100 50  0001 C CNN
F 3 "" H 6200 4100 50  0001 C CNN
	1    6200 4100
	1    0    0    1   
$EndComp
$Comp
L module_tester-v02-eagle-import:TL074P IC2
U 2 1 541775C6
P 9600 2000
AR Path="/541775C6" Ref="IC2"  Part="2" 
AR Path="/61BE22D1/541775C6" Ref="IC2"  Part="2" 
F 0 "IC2" H 9700 2125 59  0001 L BNN
F 1 "TL074P" H 9700 1800 59  0001 L BNN
F 2 "module_tester-v02:DIL14" H 9600 2000 50  0001 C CNN
F 3 "" H 9600 2000 50  0001 C CNN
	2    9600 2000
	1    0    0    1   
$EndComp
$Comp
L module_tester-v02-eagle-import:TL074P IC2
U 3 1 541775C2
P 6200 2000
AR Path="/541775C2" Ref="IC2"  Part="3" 
AR Path="/61BE22D1/541775C2" Ref="IC2"  Part="3" 
F 0 "IC2" H 6300 2125 59  0001 L BNN
F 1 "TL074P" H 6300 1800 59  0001 L BNN
F 2 "module_tester-v02:DIL14" H 6200 2000 50  0001 C CNN
F 3 "" H 6200 2000 50  0001 C CNN
	3    6200 2000
	1    0    0    1   
$EndComp
$Comp
L module_tester-v02-eagle-import:TL074P IC2
U 4 1 541775DE
P 9700 3900
AR Path="/541775DE" Ref="IC2"  Part="4" 
AR Path="/61BE22D1/541775DE" Ref="IC2"  Part="4" 
F 0 "IC2" H 9800 4025 59  0001 L BNN
F 1 "TL074P" H 9800 3700 59  0001 L BNN
F 2 "module_tester-v02:DIL14" H 9700 3900 50  0001 C CNN
F 3 "" H 9700 3900 50  0001 C CNN
	4    9700 3900
	1    0    0    1   
$EndComp
$Comp
L module_tester-v02-eagle-import:TL074P IC2
U 5 1 541775DA
P 2800 7100
AR Path="/541775DA" Ref="IC2"  Part="5" 
AR Path="/61BE22D1/541775DA" Ref="IC2"  Part="5" 
F 0 "IC2" H 2900 7225 59  0001 L BNN
F 1 "TL074P" H 2900 6900 59  0001 L BNN
F 2 "module_tester-v02:DIL14" H 2800 7100 50  0001 C CNN
F 3 "" H 2800 7100 50  0001 C CNN
	5    2800 7100
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND25
U 1 1 2EE633C3
P 5800 2300
AR Path="/2EE633C3" Ref="#GND25"  Part="1" 
AR Path="/61BE22D1/2EE633C3" Ref="#GND025"  Part="1" 
F 0 "#GND025" H 5800 2300 50  0001 C CNN
F 1 "GND" H 5700 2200 59  0000 L BNN
F 2 "" H 5800 2300 50  0001 C CNN
F 3 "" H 5800 2300 50  0001 C CNN
	1    5800 2300
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:R-TRIMM3296W R9
U 1 1 EC36DA1B
P 4500 1900
AR Path="/EC36DA1B" Ref="R9"  Part="1" 
AR Path="/61BE22D1/EC36DA1B" Ref="R9"  Part="1" 
F 0 "R9" V 4265 1750 59  0000 L BNN
F 1 "20k" V 4350 1750 59  0000 L BNN
F 2 "module_tester-v02:RTRIM3296W" H 4500 1900 50  0001 C CNN
F 3 "" H 4500 1900 50  0001 C CNN
	1    4500 1900
	0    -1   -1   0   
$EndComp
$Comp
L module_tester-v02-eagle-import:R-US_0204_7 R16
U 1 1 DB1BD950
P 6200 1200
AR Path="/DB1BD950" Ref="R16"  Part="1" 
AR Path="/61BE22D1/DB1BD950" Ref="R16"  Part="1" 
F 0 "R16" H 6050 1259 59  0000 L BNN
F 1 "150k" H 6050 1070 59  0000 L BNN
F 2 "module_tester-v02:0204_7" H 6200 1200 50  0001 C CNN
F 3 "" H 6200 1200 50  0001 C CNN
	1    6200 1200
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:R-US_0204_7 R12
U 1 1 3FEF5E80
P 5500 1300
AR Path="/3FEF5E80" Ref="R12"  Part="1" 
AR Path="/61BE22D1/3FEF5E80" Ref="R12"  Part="1" 
F 0 "R12" H 5350 1359 59  0000 L BNN
F 1 "47k" H 5350 1170 59  0000 L BNN
F 2 "module_tester-v02:0204_7" H 5500 1300 50  0001 C CNN
F 3 "" H 5500 1300 50  0001 C CNN
	1    5500 1300
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:R-US_0204_7 R10
U 1 1 77B4BF1D
P 5500 1900
AR Path="/77B4BF1D" Ref="R10"  Part="1" 
AR Path="/61BE22D1/77B4BF1D" Ref="R10"  Part="1" 
F 0 "R10" H 5350 1959 59  0000 L BNN
F 1 "82k" H 5350 1770 59  0000 L BNN
F 2 "module_tester-v02:0204_7" H 5500 1900 50  0001 C CNN
F 3 "" H 5500 1900 50  0001 C CNN
	1    5500 1900
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:R-TRIMM3296W R13
U 1 1 84733BCC
P 5000 1300
AR Path="/84733BCC" Ref="R13"  Part="1" 
AR Path="/61BE22D1/84733BCC" Ref="R13"  Part="1" 
F 0 "R13" V 4765 1150 59  0000 L BNN
F 1 "5k" V 4850 1150 59  0000 L BNN
F 2 "module_tester-v02:RTRIM3296W" H 5000 1300 50  0001 C CNN
F 3 "" H 5000 1300 50  0001 C CNN
	1    5000 1300
	0    -1   -1   0   
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND24
U 1 1 664B2FAE
P 7100 2400
AR Path="/664B2FAE" Ref="#GND24"  Part="1" 
AR Path="/61BE22D1/664B2FAE" Ref="#GND024"  Part="1" 
F 0 "#GND024" H 7100 2400 50  0001 C CNN
F 1 "GND" H 7000 2300 59  0000 L BNN
F 2 "" H 7100 2400 50  0001 C CNN
F 3 "" H 7100 2400 50  0001 C CNN
	1    7100 2400
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:TL072P IC7
U 1 1 7C0866E7
P 6200 5100
AR Path="/7C0866E7" Ref="IC7"  Part="1" 
AR Path="/61BE22D1/7C0866E7" Ref="IC7"  Part="1" 
F 0 "IC7" H 6300 5225 59  0001 L BNN
F 1 "MCP6002" H 6300 4900 59  0001 L BNN
F 2 "module_tester-v02:DIL08" H 6200 5100 50  0001 C CNN
F 3 "" H 6200 5100 50  0001 C CNN
	1    6200 5100
	1    0    0    1   
$EndComp
$Comp
L module_tester-v02-eagle-import:TL072P IC7
U 2 1 7C0866EB
P 6200 6600
AR Path="/7C0866EB" Ref="IC7"  Part="2" 
AR Path="/61BE22D1/7C0866EB" Ref="IC7"  Part="2" 
F 0 "IC7" H 6300 6725 59  0001 L BNN
F 1 "MCP6002" H 6300 6400 59  0001 L BNN
F 2 "module_tester-v02:DIL08" H 6200 6600 50  0001 C CNN
F 3 "" H 6200 6600 50  0001 C CNN
	2    6200 6600
	1    0    0    1   
$EndComp
$Comp
L module_tester-v02-eagle-import:TL072P IC7
U 3 1 7C0866EF
P 2600 7100
AR Path="/7C0866EF" Ref="IC7"  Part="3" 
AR Path="/61BE22D1/7C0866EF" Ref="IC7"  Part="3" 
F 0 "IC7" H 2700 7225 59  0001 L BNN
F 1 "MCP6002" H 2700 6900 59  0001 L BNN
F 2 "module_tester-v02:DIL08" H 2600 7100 50  0001 C CNN
F 3 "" H 2600 7100 50  0001 C CNN
	3    2600 7100
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND26
U 1 1 9E0685E6
P 5800 5400
AR Path="/9E0685E6" Ref="#GND26"  Part="1" 
AR Path="/61BE22D1/9E0685E6" Ref="#GND026"  Part="1" 
F 0 "#GND026" H 5800 5400 50  0001 C CNN
F 1 "GND" H 5700 5300 59  0000 L BNN
F 2 "" H 5800 5400 50  0001 C CNN
F 3 "" H 5800 5400 50  0001 C CNN
	1    5800 5400
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:R-US_0204_7 R32
U 1 1 25E9B245
P 5500 5000
AR Path="/25E9B245" Ref="R32"  Part="1" 
AR Path="/61BE22D1/25E9B245" Ref="R32"  Part="1" 
F 0 "R32" H 5350 5059 59  0000 L BNN
F 1 "47k" H 5350 4870 59  0000 L BNN
F 2 "module_tester-v02:0204_7" H 5500 5000 50  0001 C CNN
F 3 "" H 5500 5000 50  0001 C CNN
	1    5500 5000
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:R-US_0204_7 R30
U 1 1 DCA185BA
P 6300 4700
AR Path="/DCA185BA" Ref="R30"  Part="1" 
AR Path="/61BE22D1/DCA185BA" Ref="R30"  Part="1" 
F 0 "R30" H 6150 4759 59  0000 L BNN
F 1 "1M" H 6150 4570 59  0000 L BNN
F 2 "module_tester-v02:0204_7" H 6300 4700 50  0001 C CNN
F 3 "" H 6300 4700 50  0001 C CNN
	1    6300 4700
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:R-US_0204_7 R31
U 1 1 3D80017B
P 5400 7000
AR Path="/3D80017B" Ref="R31"  Part="1" 
AR Path="/61BE22D1/3D80017B" Ref="R31"  Part="1" 
F 0 "R31" H 5250 7059 59  0000 L BNN
F 1 "100k" H 5250 6870 59  0000 L BNN
F 2 "module_tester-v02:0204_7" H 5400 7000 50  0001 C CNN
F 3 "" H 5400 7000 50  0001 C CNN
	1    5400 7000
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:R-US_0204_7 R33
U 1 1 54FF942E
P 6000 7000
AR Path="/54FF942E" Ref="R33"  Part="1" 
AR Path="/61BE22D1/54FF942E" Ref="R33"  Part="1" 
F 0 "R33" H 5850 7059 59  0000 L BNN
F 1 "100k" H 5850 6870 59  0000 L BNN
F 2 "module_tester-v02:0204_7" H 6000 7000 50  0001 C CNN
F 3 "" H 6000 7000 50  0001 C CNN
	1    6000 7000
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND27
U 1 1 419D9C11
P 5100 7200
AR Path="/419D9C11" Ref="#GND27"  Part="1" 
AR Path="/61BE22D1/419D9C11" Ref="#GND027"  Part="1" 
F 0 "#GND027" H 5100 7200 50  0001 C CNN
F 1 "GND" H 5000 7100 59  0000 L BNN
F 2 "" H 5100 7200 50  0001 C CNN
F 3 "" H 5100 7200 50  0001 C CNN
	1    5100 7200
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:+5V #P+1
U 1 1 BCDF72C5
P 6400 6900
AR Path="/BCDF72C5" Ref="#P+1"  Part="1" 
AR Path="/61BE22D1/BCDF72C5" Ref="#P+01"  Part="1" 
F 0 "#P+01" H 6400 6900 50  0001 C CNN
F 1 "+5V" V 6300 6700 59  0000 L BNN
F 2 "" H 6400 6900 50  0001 C CNN
F 3 "" H 6400 6900 50  0001 C CNN
	1    6400 6900
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:CPOL-USE2.5-6 C24
U 1 1 E783D9EE
P 5200 5900
AR Path="/E783D9EE" Ref="C24"  Part="1" 
AR Path="/61BE22D1/E783D9EE" Ref="C24"  Part="1" 
F 0 "C24" H 5240 5925 59  0000 L BNN
F 1 "4.7u" H 5240 5735 59  0000 L BNN
F 2 "module_tester-v02:E2,5-6" H 5200 5900 50  0001 C CNN
F 3 "" H 5200 5900 50  0001 C CNN
	1    5200 5900
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:R-US_0204_7 R29
U 1 1 0B6F6D7A
P 5500 6500
AR Path="/0B6F6D7A" Ref="R29"  Part="1" 
AR Path="/61BE22D1/0B6F6D7A" Ref="R29"  Part="1" 
F 0 "R29" H 5350 6559 59  0000 L BNN
F 1 "100k" H 5350 6370 59  0000 L BNN
F 2 "module_tester-v02:0204_7" H 5500 6500 50  0001 C CNN
F 3 "" H 5500 6500 50  0001 C CNN
	1    5500 6500
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:R-US_0204_7 R28
U 1 1 F6B01F28
P 6200 6100
AR Path="/F6B01F28" Ref="R28"  Part="1" 
AR Path="/61BE22D1/F6B01F28" Ref="R28"  Part="1" 
F 0 "R28" H 6050 6159 59  0000 L BNN
F 1 "33k" H 6050 5970 59  0000 L BNN
F 2 "module_tester-v02:0204_7" H 6200 6100 50  0001 C CNN
F 3 "" H 6200 6100 50  0001 C CNN
	1    6200 6100
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:C-US025-025X050 C25
U 1 1 1DB35DCC
P 5700 7200
AR Path="/1DB35DCC" Ref="C25"  Part="1" 
AR Path="/61BE22D1/1DB35DCC" Ref="C25"  Part="1" 
F 0 "C25" H 5740 7225 59  0000 L BNN
F 1 "100n" H 5740 7035 59  0000 L BNN
F 2 "module_tester-v02:C025-025X050" H 5700 7200 50  0001 C CNN
F 3 "" H 5700 7200 50  0001 C CNN
	1    5700 7200
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND34
U 1 1 0FD0C583
P 5700 7600
AR Path="/0FD0C583" Ref="#GND34"  Part="1" 
AR Path="/61BE22D1/0FD0C583" Ref="#GND034"  Part="1" 
F 0 "#GND034" H 5700 7600 50  0001 C CNN
F 1 "GND" H 5600 7500 59  0000 L BNN
F 2 "" H 5700 7600 50  0001 C CNN
F 3 "" H 5700 7600 50  0001 C CNN
	1    5700 7600
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND35
U 1 1 CF03C53C
P 2600 7700
AR Path="/CF03C53C" Ref="#GND35"  Part="1" 
AR Path="/61BE22D1/CF03C53C" Ref="#GND035"  Part="1" 
F 0 "#GND035" H 2600 7700 50  0001 C CNN
F 1 "GND" H 2500 7600 59  0000 L BNN
F 2 "" H 2600 7700 50  0001 C CNN
F 3 "" H 2600 7700 50  0001 C CNN
	1    2600 7700
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:+5V #P+13
U 1 1 9B33CC1C
P 2600 6500
AR Path="/9B33CC1C" Ref="#P+13"  Part="1" 
AR Path="/61BE22D1/9B33CC1C" Ref="#P+013"  Part="1" 
F 0 "#P+013" H 2600 6500 50  0001 C CNN
F 1 "+5V" V 2500 6300 59  0000 L BNN
F 2 "" H 2600 6500 50  0001 C CNN
F 3 "" H 2600 6500 50  0001 C CNN
	1    2600 6500
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:C-US025-025X050 C23
U 1 1 723FB792
P 2300 7100
AR Path="/723FB792" Ref="C23"  Part="1" 
AR Path="/61BE22D1/723FB792" Ref="C23"  Part="1" 
F 0 "C23" H 2340 7125 59  0000 L BNN
F 1 "100n" H 2340 6935 59  0000 L BNN
F 2 "module_tester-v02:C025-025X050" H 2300 7100 50  0001 C CNN
F 3 "" H 2300 7100 50  0001 C CNN
	1    2300 7100
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:R-US_0204_7 R6
U 1 1 96E7E6D7
P 5000 1900
AR Path="/96E7E6D7" Ref="R6"  Part="1" 
AR Path="/61BE22D1/96E7E6D7" Ref="R6"  Part="1" 
F 0 "R6" H 4850 1959 59  0000 L BNN
F 1 "10k" H 4850 1770 59  0000 L BNN
F 2 "module_tester-v02:0204_7" H 5000 1900 50  0001 C CNN
F 3 "" H 5000 1900 50  0001 C CNN
	1    5000 1900
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND37
U 1 1 7CFE796D
P 9300 4200
AR Path="/7CFE796D" Ref="#GND37"  Part="1" 
AR Path="/61BE22D1/7CFE796D" Ref="#GND037"  Part="1" 
F 0 "#GND037" H 9300 4200 50  0001 C CNN
F 1 "GND" H 9200 4100 59  0000 L BNN
F 2 "" H 9300 4200 50  0001 C CNN
F 3 "" H 9300 4200 50  0001 C CNN
	1    9300 4200
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:+5V #P+3
U 1 1 00F05DF6
P 2400 700
AR Path="/00F05DF6" Ref="#P+3"  Part="1" 
AR Path="/61BE22D1/00F05DF6" Ref="#P+03"  Part="1" 
F 0 "#P+03" H 2400 700 50  0001 C CNN
F 1 "+5V" V 2300 500 59  0000 L BNN
F 2 "" H 2400 700 50  0001 C CNN
F 3 "" H 2400 700 50  0001 C CNN
	1    2400 700 
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:C-US025-025X050 C8
U 1 1 8EFB4A12
P 6100 1600
AR Path="/8EFB4A12" Ref="C8"  Part="1" 
AR Path="/61BE22D1/8EFB4A12" Ref="C8"  Part="1" 
F 0 "C8" H 6140 1625 59  0000 L BNN
F 1 "1n" H 6140 1435 59  0000 L BNN
F 2 "module_tester-v02:C025-025X050" H 6100 1600 50  0001 C CNN
F 3 "" H 6100 1600 50  0001 C CNN
	1    6100 1600
	0    -1   -1   0   
$EndComp
$Comp
L module_tester-v02-eagle-import:C-US025-025X050 C22
U 1 1 8A2FD79C
P 6100 5800
AR Path="/8A2FD79C" Ref="C22"  Part="1" 
AR Path="/61BE22D1/8A2FD79C" Ref="C22"  Part="1" 
F 0 "C22" H 6140 5825 59  0000 L BNN
F 1 "1n" H 6140 5635 59  0000 L BNN
F 2 "module_tester-v02:C025-025X050" H 6100 5800 50  0001 C CNN
F 3 "" H 6100 5800 50  0001 C CNN
	1    6100 5800
	0    -1   -1   0   
$EndComp
$Comp
L module_tester-v02-eagle-import:CPOL-USE2.5-7 C9
U 1 1 C2FDBD7A
P 2000 7100
AR Path="/C2FDBD7A" Ref="C9"  Part="1" 
AR Path="/61BE22D1/C2FDBD7A" Ref="C9"  Part="1" 
F 0 "C9" H 2040 7125 59  0000 L BNN
F 1 "100u" H 2040 6935 59  0000 L BNN
F 2 "module_tester-v02:E2,5-7" H 2000 7100 50  0001 C CNN
F 3 "" H 2000 7100 50  0001 C CNN
	1    2000 7100
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:PJ301_THONKICONN6 J3
U 1 1 D6A0AFFD
P 7400 2100
AR Path="/D6A0AFFD" Ref="J3"  Part="1" 
AR Path="/61BE22D1/D6A0AFFD" Ref="J3"  Part="1" 
F 0 "J3" H 7300 2260 59  0000 L BNN
F 1 "PJ301_THONKICONN6" H 7400 2100 50  0001 C CNN
F 2 "module_tester-v02:WQP_PJ_301M6" H 7400 2100 50  0001 C CNN
F 3 "" H 7400 2100 50  0001 C CNN
	1    7400 2100
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:PJ301_THONKICONN6 J5
U 1 1 0A445BA9
P 4700 4100
AR Path="/0A445BA9" Ref="J5"  Part="1" 
AR Path="/61BE22D1/0A445BA9" Ref="J5"  Part="1" 
F 0 "J5" H 4600 4260 59  0000 L BNN
F 1 "PJ301_THONKICONN6" H 4700 4100 50  0001 C CNN
F 2 "module_tester-v02:WQP_PJ_301M6" H 4700 4100 50  0001 C CNN
F 3 "" H 4700 4100 50  0001 C CNN
	1    4700 4100
	-1   0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:PJ301_THONKICONN6 J4
U 1 1 2A89DB3B
P 10800 2100
AR Path="/2A89DB3B" Ref="J4"  Part="1" 
AR Path="/61BE22D1/2A89DB3B" Ref="J4"  Part="1" 
F 0 "J4" H 10700 2260 59  0000 L BNN
F 1 "PJ301_THONKICONN6" H 10800 2100 50  0001 C CNN
F 2 "module_tester-v02:WQP_PJ_301M6" H 10800 2100 50  0001 C CNN
F 3 "" H 10800 2100 50  0001 C CNN
	1    10800 2100
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:R-US_0204_7 R34
U 1 1 B952D5C6
P 6800 2000
AR Path="/B952D5C6" Ref="R34"  Part="1" 
AR Path="/61BE22D1/B952D5C6" Ref="R34"  Part="1" 
F 0 "R34" H 6650 2059 59  0000 L BNN
F 1 "470" H 6650 1870 59  0000 L BNN
F 2 "module_tester-v02:0204_7" H 6800 2000 50  0001 C CNN
F 3 "" H 6800 2000 50  0001 C CNN
	1    6800 2000
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:R-US_0204_7 R35
U 1 1 210866ED
P 10200 2000
AR Path="/210866ED" Ref="R35"  Part="1" 
AR Path="/61BE22D1/210866ED" Ref="R35"  Part="1" 
F 0 "R35" H 10050 2059 59  0000 L BNN
F 1 "470" H 10050 1870 59  0000 L BNN
F 2 "module_tester-v02:0204_7" H 10200 2000 50  0001 C CNN
F 3 "" H 10200 2000 50  0001 C CNN
	1    10200 2000
	1    0    0    -1  
$EndComp
Text Notes 7800 7600 0    127  ~ 25
AD and DA
Text Notes 7800 7800 0    85   ~ 0
Émilie Gillet, 2012. cc-by-sa
$EndSCHEMATC
