# module_tester-v02.sch BOM

Sat 18 Dec 2021 08:43:50 AM EST

Generated from schematic by Eeschema 5.1.12-84ad8e8a86~92~ubuntu20.04.1

**Component Count:** 107

| Refs | Qty | Component | Description |
| ----- | --- | ---- | ----------- |
| C1, C7, C13, C14 | 4 | 18p |  |
| C2, C3, C4, C5, C11, C15, C16, C17, C18, C19, C20, C21, C23, C25 | 14 | 100n |  |
| C6, C24 | 2 | 4.7u |  |
| C8, C22 | 2 | 1n |  |
| C9, C26, C27, C28 | 4 | 100u |  |
| D1 | 1 | 1N4148 |  |
| D8 | 1 | 1N5819 |  |
| D9 | 1 | 1N4004 |  |
| IC1 | 1 | LM4040 4.1V |  |
| IC2 | 1 | TL074P |  |
| IC3 | 1 | MCP4822 |  |
| IC4 | 1 | ATMEGA644p |  |
| IC5 | 1 | 74595N |  |
| IC6 | 1 | 74HC165N |  |
| IC7 | 1 | MCP6002 |  |
| IC8 | 1 | 7805 |  |
| J1, J2, J3, J4, J5, J10 | 6 | PJ301_THONKICONN6 |  |
| J7 | 1 | M08X2 |  |
| J8 | 1 | MONOSWSLIM |  |
| J9 | 1 | JACK-PLUG2 |  |
| JP3, JP4 | 2 | M02PTH |  |
| JP6, JP7, JP8, JP9 | 4 | M01PTH |  |
| LED1, LED2, LED3, LED4, LED5, LED6 | 6 | LED3MM |  |
| OK1 | 1 | 6N137 |  |
| Q1 | 1 | 2N3904 |  |
| Q2 | 1 | 20MHz |  |
| R1, R2, R34, R35 | 4 | 470 |  |
| R3, R19, R20, R21, R22, R23, R24, R25, R27 | 9 | 220 |  |
| R4, R28 | 2 | 33k |  |
| R5, R6, R17, R26 | 4 | 10k |  |
| R7, R18, R29, R31, R33 | 5 | 100k |  |
| R8 | 1 | 5k |  |
| R9 | 1 | 20k |  |
| R10 | 1 | 82k |  |
| R11 | 1 | 62k |  |
| R12, R32 | 2 | 47k |  |
| R13 | 1 | 5k |  |
| R14 | 1 | 0 |  |
| R15, R16 | 2 | 150k |  |
| R30 | 1 | 1M |  |
| RN1 | 1 | G06R |  |
| S1, S2, S3, S4, S5, S6 | 6 | 40-XX |  |
| SW1 | 1 | Encoder |  |
| U1 | 1 | ISP |  |
| U$3 | 1 | LCD_MODULE2X16 |  |
| X1 | 1 | MIDI |  |
    
