EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 12505 8387
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Bus Line
	6900 1600 6900 1300
Wire Bus Line
	6900 1300 7000 1200
Wire Bus Line
	7000 1200 7900 1200
Text Label 6900 1450 0    70   ~ 0
LCD_DATA[4..7]
Wire Wire Line
	8900 2600 8800 2600
Text GLabel 8900 2600 0    10   BiDi ~ 0
GND
Wire Wire Line
	4300 2500 4300 2600
Text GLabel 4300 2500 0    10   BiDi ~ 0
GND
Wire Wire Line
	3300 4000 3300 4100
Text GLabel 3300 4000 0    10   BiDi ~ 0
GND
Wire Wire Line
	5000 2700 5000 2600
Wire Wire Line
	5000 2600 5100 2600
Wire Wire Line
	5100 2300 5000 2300
Wire Wire Line
	5000 2600 5000 2300
Connection ~ 5000 2600
Text GLabel 5000 2700 0    10   BiDi ~ 0
GND
Wire Wire Line
	4000 2000 3900 2000
Wire Wire Line
	4000 1600 3900 1600
Wire Wire Line
	3900 2000 3900 1800
Wire Wire Line
	3900 1800 3900 1600
Wire Wire Line
	3900 1800 3800 1800
Wire Wire Line
	3800 1800 3800 1900
Connection ~ 3900 1800
Text GLabel 4000 2000 0    10   BiDi ~ 0
GND
Wire Wire Line
	4400 4100 4400 4300
Wire Wire Line
	4400 4300 4700 4300
Wire Wire Line
	4700 4300 4700 4100
Wire Wire Line
	4400 4300 4400 4400
Connection ~ 4400 4300
Text GLabel 4400 4100 0    10   BiDi ~ 0
GND
Wire Wire Line
	8100 2600 8000 2600
Text Label 8000 2600 2    70   ~ 0
RESET
Wire Wire Line
	5100 1400 5000 1400
Wire Wire Line
	5000 1400 5000 1300
Wire Wire Line
	4900 1400 5000 1400
Connection ~ 5000 1400
Text Label 4900 1400 2    70   ~ 0
RESET
Wire Wire Line
	3300 3700 3300 3600
Text Label 3300 3600 0    70   ~ 0
RESET
Wire Wire Line
	6700 2400 8100 2400
Text Label 6800 2400 0    70   ~ 0
MISO
Wire Wire Line
	4300 2000 4400 2000
Wire Wire Line
	4400 2000 4400 1900
Wire Wire Line
	4400 2000 4800 2000
Wire Wire Line
	4800 2000 4800 1900
Wire Wire Line
	4800 1900 5100 1900
Connection ~ 4400 2000
Wire Wire Line
	4300 1600 4400 1600
Wire Wire Line
	4400 1600 4400 1700
Wire Wire Line
	4400 1600 4800 1600
Wire Wire Line
	4800 1600 4800 1700
Wire Wire Line
	4800 1700 5100 1700
Connection ~ 4400 1600
Wire Wire Line
	8800 2500 9100 2500
Wire Wire Line
	9100 2500 9100 2900
Wire Wire Line
	9100 2900 7600 2900
Wire Wire Line
	7600 2900 7600 2500
Wire Wire Line
	7600 2500 6700 2500
Text GLabel 6800 2500 2    70   BiDi ~ 0
MOSI
Wire Wire Line
	4300 2200 4300 2100
Wire Wire Line
	4300 2100 5100 2100
Wire Wire Line
	6700 1700 6800 1700
Text GLabel 6800 1700 0    10   BiDi ~ 0
LCD_DATA4
Wire Wire Line
	6700 2300 7800 2300
Wire Wire Line
	7800 2300 7800 2500
Wire Wire Line
	7800 2500 8100 2500
Text GLabel 6800 2300 2    70   BiDi ~ 0
SCK
Wire Wire Line
	6700 1600 6800 1600
Text GLabel 6800 1600 0    10   BiDi ~ 0
LCD_DATA5
Wire Wire Line
	6700 1500 6800 1500
Text GLabel 6800 1500 0    10   BiDi ~ 0
LCD_DATA6
Wire Wire Line
	6700 1400 6800 1400
Text GLabel 6800 1400 0    10   BiDi ~ 0
LCD_DATA7
Wire Wire Line
	6700 3500 6800 3500
Text GLabel 6800 3500 2    70   BiDi ~ 0
IO_ENABLE
Wire Wire Line
	6700 3400 6800 3400
Text GLabel 6800 3400 2    70   BiDi ~ 0
IO_OUTPUT
Wire Wire Line
	6700 3300 6800 3300
Text GLabel 6800 3300 2    70   BiDi ~ 0
IO_INPUT
Wire Wire Line
	6800 3200 6700 3200
Text GLabel 6800 3200 2    70   BiDi ~ 0
IO_SCK
Wire Wire Line
	5000 800  5000 900 
Text GLabel 5000 800  0    10   BiDi ~ 0
+5V
Wire Wire Line
	5100 2200 4900 2200
Wire Wire Line
	4900 2200 4900 2500
Wire Wire Line
	4900 2500 5100 2500
Wire Wire Line
	4900 2500 4800 2500
Wire Wire Line
	4800 2500 4800 2400
Connection ~ 4900 2500
Text GLabel 5100 2200 0    10   BiDi ~ 0
+5V
Wire Wire Line
	8800 2400 8900 2400
Wire Wire Line
	8900 2400 8900 2300
Text GLabel 8800 2400 0    10   BiDi ~ 0
+5V
Wire Wire Line
	4400 3800 4400 3600
Wire Wire Line
	4400 3500 4400 3600
Wire Wire Line
	4400 3600 4700 3600
Wire Wire Line
	4700 3600 4700 3800
Connection ~ 4400 3600
Text GLabel 4400 3800 0    10   BiDi ~ 0
+5V
Wire Wire Line
	6700 2100 6800 2100
Text GLabel 6800 2100 2    70   BiDi ~ 0
ADC
Wire Wire Line
	6700 4800 6800 4800
Text GLabel 6800 4800 2    70   BiDi ~ 0
MIDI_IN
Wire Wire Line
	6700 1800 6800 1800
Text GLabel 6800 1800 2    70   BiDi ~ 0
LCD_RS
Wire Wire Line
	6700 1900 6800 1900
Text GLabel 6800 1900 2    70   BiDi ~ 0
LCD_ENABLE
Wire Wire Line
	6700 4200 6800 4200
Text GLabel 6800 4200 2    70   BiDi ~ 0
PITCH_DETECTOR
Wire Wire Line
	6700 2600 6800 2600
Text GLabel 6800 2600 2    70   BiDi ~ 0
SS
Wire Wire Line
	6700 3700 6800 3700
Text GLabel 6800 3700 2    70   BiDi ~ 0
ENC_A
Wire Wire Line
	6700 3800 6800 3800
Text GLabel 6800 3800 2    70   BiDi ~ 0
ENC_B
Wire Wire Line
	6700 3900 6800 3900
Text GLabel 6800 3900 2    70   BiDi ~ 0
ENC_CLICK
Wire Wire Line
	6700 4400 6800 4400
Text GLabel 6800 4400 2    70   BiDi ~ 0
CLOCK_OUT
Wire Wire Line
	6700 4500 6800 4500
Text GLabel 6800 4500 2    70   BiDi ~ 0
GATE_OUT
Wire Wire Line
	6700 4600 6800 4600
Text GLabel 6800 4600 2    70   BiDi ~ 0
GATE_IN
Entry Wire Line
	6800 1700 6900 1600
Entry Wire Line
	6800 1600 6900 1500
Entry Wire Line
	6800 1500 6900 1400
Entry Wire Line
	6800 1400 6900 1300
$Comp
L module_tester-v02-eagle-import:AVR_SPI_PRG_6PTH U1
U 1 1 9E4F24D8
P 8400 2500
AR Path="/9E4F24D8" Ref="U1"  Part="1" 
AR Path="/61BE1D02/9E4F24D8" Ref="U1"  Part="1" 
F 0 "U1" H 8230 2730 59  0000 L BNN
F 1 "ISP" H 8240 2200 59  0000 L BNN
F 2 "module_tester-v02:2X3" H 8400 2500 50  0001 C CNN
F 3 "" H 8400 2500 50  0001 C CNN
	1    8400 2500
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:MEGA16-P IC4
U 1 1 4DFBBE8F
P 5900 3000
AR Path="/4DFBBE8F" Ref="IC4"  Part="1" 
AR Path="/61BE1D02/4DFBBE8F" Ref="IC4"  Part="1" 
F 0 "IC4" H 5300 4730 59  0000 L BNN
F 1 "ATMEGA644p" H 5300 1000 59  0000 L BNN
F 2 "module_tester-v02:DIL40" H 5900 3000 50  0001 C CNN
F 3 "" H 5900 3000 50  0001 C CNN
	1    5900 3000
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:R-US_0204_7 R26
U 1 1 49EEB110
P 5000 1100
AR Path="/49EEB110" Ref="R26"  Part="1" 
AR Path="/61BE1D02/49EEB110" Ref="R26"  Part="1" 
F 0 "R26" H 4850 1159 59  0000 L BNN
F 1 "10k" H 4850 970 59  0000 L BNN
F 2 "module_tester-v02:0204_7" H 5000 1100 50  0001 C CNN
F 3 "" H 5000 1100 50  0001 C CNN
	1    5000 1100
	0    -1   -1   0   
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND3
U 1 1 7679BDCC
P 8900 2700
AR Path="/7679BDCC" Ref="#GND3"  Part="1" 
AR Path="/61BE1D02/7679BDCC" Ref="#GND03"  Part="1" 
F 0 "#GND03" H 8900 2700 50  0001 C CNN
F 1 "GND" H 8800 2600 59  0000 L BNN
F 2 "" H 8900 2700 50  0001 C CNN
F 3 "" H 8900 2700 50  0001 C CNN
	1    8900 2700
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND13
U 1 1 9E3D6951
P 5000 2800
AR Path="/9E3D6951" Ref="#GND13"  Part="1" 
AR Path="/61BE1D02/9E3D6951" Ref="#GND013"  Part="1" 
F 0 "#GND013" H 5000 2800 50  0001 C CNN
F 1 "GND" H 4900 2700 59  0000 L BNN
F 2 "" H 5000 2800 50  0001 C CNN
F 3 "" H 5000 2800 50  0001 C CNN
	1    5000 2800
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:CRYSTALHC49U-V Q2
U 1 1 EC524F1E
P 4400 1800
AR Path="/EC524F1E" Ref="Q2"  Part="1" 
AR Path="/61BE1D02/EC524F1E" Ref="Q2"  Part="1" 
F 0 "Q2" H 4500 1840 59  0000 L BNN
F 1 "20MHz" H 4500 1700 59  0000 L BNN
F 2 "module_tester-v02:HC49U-V" H 4400 1800 50  0001 C CNN
F 3 "" H 4400 1800 50  0001 C CNN
	1    4400 1800
	0    -1   -1   0   
$EndComp
$Comp
L module_tester-v02-eagle-import:C-US025-025X050 C14
U 1 1 093E056A
P 4100 1600
AR Path="/093E056A" Ref="C14"  Part="1" 
AR Path="/61BE1D02/093E056A" Ref="C14"  Part="1" 
F 0 "C14" H 4140 1625 59  0000 L BNN
F 1 "18p" H 4140 1435 59  0000 L BNN
F 2 "module_tester-v02:C025-025X050" H 4100 1600 50  0001 C CNN
F 3 "" H 4100 1600 50  0001 C CNN
	1    4100 1600
	0    -1   -1   0   
$EndComp
$Comp
L module_tester-v02-eagle-import:C-US025-025X050 C13
U 1 1 EDEBC786
P 4100 2000
AR Path="/EDEBC786" Ref="C13"  Part="1" 
AR Path="/61BE1D02/EDEBC786" Ref="C13"  Part="1" 
F 0 "C13" H 4140 2025 59  0000 L BNN
F 1 "18p" H 4140 1835 59  0000 L BNN
F 2 "module_tester-v02:C025-025X050" H 4100 2000 50  0001 C CNN
F 3 "" H 4100 2000 50  0001 C CNN
	1    4100 2000
	0    -1   -1   0   
$EndComp
$Comp
L module_tester-v02-eagle-import:C-US025-025X050 C18
U 1 1 52B654BE
P 4300 2300
AR Path="/52B654BE" Ref="C18"  Part="1" 
AR Path="/61BE1D02/52B654BE" Ref="C18"  Part="1" 
F 0 "C18" H 4340 2325 59  0000 L BNN
F 1 "100n" H 4340 2135 59  0000 L BNN
F 2 "module_tester-v02:C025-025X050" H 4300 2300 50  0001 C CNN
F 3 "" H 4300 2300 50  0001 C CNN
	1    4300 2300
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND17
U 1 1 0A5A0AC8
P 4300 2700
AR Path="/0A5A0AC8" Ref="#GND17"  Part="1" 
AR Path="/61BE1D02/0A5A0AC8" Ref="#GND017"  Part="1" 
F 0 "#GND017" H 4300 2700 50  0001 C CNN
F 1 "GND" H 4200 2600 59  0000 L BNN
F 2 "" H 4300 2700 50  0001 C CNN
F 3 "" H 4300 2700 50  0001 C CNN
	1    4300 2700
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:C-US025-025X050 C20
U 1 1 1D7B263C
P 3300 3800
AR Path="/1D7B263C" Ref="C20"  Part="1" 
AR Path="/61BE1D02/1D7B263C" Ref="C20"  Part="1" 
F 0 "C20" H 3340 3825 59  0000 L BNN
F 1 "100n" H 3340 3635 59  0000 L BNN
F 2 "module_tester-v02:C025-025X050" H 3300 3800 50  0001 C CNN
F 3 "" H 3300 3800 50  0001 C CNN
	1    3300 3800
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND21
U 1 1 CF593DC2
P 3300 4200
AR Path="/CF593DC2" Ref="#GND21"  Part="1" 
AR Path="/61BE1D02/CF593DC2" Ref="#GND021"  Part="1" 
F 0 "#GND021" H 3300 4200 50  0001 C CNN
F 1 "GND" H 3200 4100 59  0000 L BNN
F 2 "" H 3300 4200 50  0001 C CNN
F 3 "" H 3300 4200 50  0001 C CNN
	1    3300 4200
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND5
U 1 1 EF27308A
P 3800 2000
AR Path="/EF27308A" Ref="#GND5"  Part="1" 
AR Path="/61BE1D02/EF27308A" Ref="#GND05"  Part="1" 
F 0 "#GND05" H 3800 2000 50  0001 C CNN
F 1 "GND" H 3700 1900 59  0000 L BNN
F 2 "" H 3800 2000 50  0001 C CNN
F 3 "" H 3800 2000 50  0001 C CNN
	1    3800 2000
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:C-US025-025X050 C16
U 1 1 E207C13D
P 4400 3900
AR Path="/E207C13D" Ref="C16"  Part="1" 
AR Path="/61BE1D02/E207C13D" Ref="C16"  Part="1" 
F 0 "C16" H 4440 3925 59  0000 L BNN
F 1 "100n" H 4440 3735 59  0000 L BNN
F 2 "module_tester-v02:C025-025X050" H 4400 3900 50  0001 C CNN
F 3 "" H 4400 3900 50  0001 C CNN
	1    4400 3900
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:C-US025-025X050 C11
U 1 1 59663956
P 4700 3900
AR Path="/59663956" Ref="C11"  Part="1" 
AR Path="/61BE1D02/59663956" Ref="C11"  Part="1" 
F 0 "C11" H 4740 3925 59  0000 L BNN
F 1 "100n" H 4740 3735 59  0000 L BNN
F 2 "module_tester-v02:C025-025X050" H 4700 3900 50  0001 C CNN
F 3 "" H 4700 3900 50  0001 C CNN
	1    4700 3900
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND14
U 1 1 109F320A
P 4400 4500
AR Path="/109F320A" Ref="#GND14"  Part="1" 
AR Path="/61BE1D02/109F320A" Ref="#GND014"  Part="1" 
F 0 "#GND014" H 4400 4500 50  0001 C CNN
F 1 "GND" H 4300 4400 59  0000 L BNN
F 2 "" H 4400 4500 50  0001 C CNN
F 3 "" H 4400 4500 50  0001 C CNN
	1    4400 4500
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:LETTER_L #FRAME6
U 2 1 70D35453
P 7700 7500
AR Path="/70D35453" Ref="#FRAME6"  Part="2" 
AR Path="/61BE1D02/70D35453" Ref="#FRAME6"  Part="2" 
F 0 "#FRAME6" H 7700 7500 50  0001 C CNN
F 1 "LETTER_L" H 7700 7500 50  0001 C CNN
F 2 "" H 7700 7500 50  0001 C CNN
F 3 "" H 7700 7500 50  0001 C CNN
	2    7700 7500
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:FRAME_A_L #FRAME5
U 1 1 FD6EE4A8
P 900 7500
AR Path="/FD6EE4A8" Ref="#FRAME5"  Part="1" 
AR Path="/61BE1D02/FD6EE4A8" Ref="#FRAME5"  Part="1" 
F 0 "#FRAME5" H 900 7500 50  0001 C CNN
F 1 "FRAME_A_L" H 900 7500 50  0001 C CNN
F 2 "" H 900 7500 50  0001 C CNN
F 3 "" H 900 7500 50  0001 C CNN
	1    900  7500
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:FRAME_A_L #FRAME5
U 2 1 FD6EE4A4
P 7700 7500
AR Path="/FD6EE4A4" Ref="#FRAME5"  Part="2" 
AR Path="/61BE1D02/FD6EE4A4" Ref="#FRAME5"  Part="2" 
F 0 "#FRAME5" H 7700 7500 50  0001 C CNN
F 1 "FRAME_A_L" H 7700 7500 50  0001 C CNN
F 2 "" H 7700 7500 50  0001 C CNN
F 3 "" H 7700 7500 50  0001 C CNN
	2    7700 7500
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:+5V #P+35
U 1 1 4C62D843
P 5000 700
AR Path="/4C62D843" Ref="#P+35"  Part="1" 
AR Path="/61BE1D02/4C62D843" Ref="#P+035"  Part="1" 
F 0 "#P+035" H 5000 700 50  0001 C CNN
F 1 "+5V" V 4900 500 59  0000 L BNN
F 2 "" H 5000 700 50  0001 C CNN
F 3 "" H 5000 700 50  0001 C CNN
	1    5000 700 
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:+5V #P+2
U 1 1 339DE9D3
P 4800 2300
AR Path="/339DE9D3" Ref="#P+2"  Part="1" 
AR Path="/61BE1D02/339DE9D3" Ref="#P+02"  Part="1" 
F 0 "#P+02" H 4800 2300 50  0001 C CNN
F 1 "+5V" V 4700 2100 59  0000 L BNN
F 2 "" H 4800 2300 50  0001 C CNN
F 3 "" H 4800 2300 50  0001 C CNN
	1    4800 2300
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:+5V #P+10
U 1 1 99CC31BE
P 4400 3400
AR Path="/99CC31BE" Ref="#P+10"  Part="1" 
AR Path="/61BE1D02/99CC31BE" Ref="#P+010"  Part="1" 
F 0 "#P+010" H 4400 3400 50  0001 C CNN
F 1 "+5V" V 4300 3200 59  0000 L BNN
F 2 "" H 4400 3400 50  0001 C CNN
F 3 "" H 4400 3400 50  0001 C CNN
	1    4400 3400
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:+5V #P+4
U 1 1 A7FDB2D8
P 8900 2200
AR Path="/A7FDB2D8" Ref="#P+4"  Part="1" 
AR Path="/61BE1D02/A7FDB2D8" Ref="#P+04"  Part="1" 
F 0 "#P+04" H 8900 2200 50  0001 C CNN
F 1 "+5V" V 8800 2000 59  0000 L BNN
F 2 "" H 8900 2200 50  0001 C CNN
F 3 "" H 8900 2200 50  0001 C CNN
	1    8900 2200
	1    0    0    -1  
$EndComp
Text Notes 7800 6300 0    127  ~ 25
Main MCU
Text Notes 7800 6500 0    85   ~ 0
Émilie Gillet, 2012. cc-by-sa
$EndSCHEMATC
