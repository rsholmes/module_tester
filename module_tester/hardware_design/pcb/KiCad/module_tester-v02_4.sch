EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 12519 8268
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	8300 3900 8300 4100
Text GLabel 8300 3900 0    10   BiDi ~ 0
GND
Wire Wire Line
	8600 4000 8600 4100
Text GLabel 8600 4000 0    10   BiDi ~ 0
GND
Wire Wire Line
	9200 2100 9300 2100
Wire Wire Line
	9300 2100 9300 2000
Wire Wire Line
	9300 2000 9300 1900
Wire Wire Line
	9300 1900 9200 1900
Wire Wire Line
	9200 2000 9300 2000
Wire Wire Line
	8600 1900 9200 1900
Wire Wire Line
	8600 2000 9200 2000
Wire Wire Line
	8600 2100 9200 2100
Wire Wire Line
	9300 2000 9800 2000
Connection ~ 9300 1900
Connection ~ 9300 2000
Connection ~ 9300 2100
Connection ~ 9200 1900
Connection ~ 9200 2000
Connection ~ 9200 2100
Text GLabel 9200 2100 0    10   BiDi ~ 0
GND
Wire Wire Line
	5800 3200 5900 3200
Wire Wire Line
	5900 3200 5900 3500
Wire Wire Line
	5900 3500 5900 3700
Wire Wire Line
	5900 3700 5800 3700
Wire Wire Line
	5900 3500 6200 3500
Wire Wire Line
	6200 3500 6200 3600
Wire Wire Line
	6500 3400 6500 3500
Wire Wire Line
	6500 3500 6500 3600
Wire Wire Line
	6200 3500 6500 3500
Connection ~ 5900 3500
Connection ~ 6200 3500
Connection ~ 6500 3500
Text GLabel 5800 3200 0    10   BiDi ~ 0
GND
Wire Wire Line
	6400 1900 6400 1800
Wire Wire Line
	5900 1900 6400 1900
Wire Wire Line
	5900 1700 5900 1900
Wire Wire Line
	5400 1900 5900 1900
Wire Wire Line
	5400 1700 5400 1900
Wire Wire Line
	5400 1900 5400 2000
Wire Wire Line
	4900 1900 5400 1900
Wire Wire Line
	4900 1800 4900 1900
Connection ~ 5900 1900
Connection ~ 5400 1900
Text GLabel 6400 1900 0    10   BiDi ~ 0
GND
Wire Wire Line
	6400 1400 6400 1200
Wire Wire Line
	6400 1500 6400 1400
Wire Wire Line
	6400 1400 5900 1400
Wire Wire Line
	5900 1400 5800 1400
Wire Wire Line
	5800 1400 5700 1400
Wire Wire Line
	5500 1100 5800 1100
Wire Wire Line
	5800 1100 5800 1400
Wire Wire Line
	5900 1500 5900 1400
Connection ~ 6400 1400
Connection ~ 5800 1400
Connection ~ 5900 1400
Text GLabel 6400 1400 0    10   BiDi ~ 0
+5V
Wire Wire Line
	9200 1700 9300 1700
Wire Wire Line
	8600 1700 9200 1700
Wire Wire Line
	9300 1700 9500 1700
Wire Wire Line
	9500 1700 9500 1600
Connection ~ 9300 1700
Connection ~ 9200 1700
Text GLabel 9200 1700 0    10   BiDi ~ 0
+5V
Wire Wire Line
	9300 1800 9200 1800
Wire Wire Line
	9200 1800 8600 1800
Wire Wire Line
	9300 1800 9700 1800
Connection ~ 9300 1800
Connection ~ 9200 1800
Text GLabel 9300 1800 0    10   BiDi ~ 0
VCC
Wire Wire Line
	6500 3100 6500 2800
Text GLabel 6500 3100 0    10   BiDi ~ 0
VCC
Wire Wire Line
	5800 2700 5900 2700
Wire Wire Line
	5900 2700 5900 2600
Text GLabel 5800 2700 0    10   BiDi ~ 0
VCC
Wire Wire Line
	4900 1400 5100 1400
Wire Wire Line
	5300 1100 4900 1100
Wire Wire Line
	4900 1100 4900 1400
Wire Wire Line
	4900 1400 4900 1500
Wire Wire Line
	4900 1400 4700 1400
Wire Wire Line
	4700 1400 4700 1300
Connection ~ 4900 1400
Text GLabel 4900 1400 0    10   BiDi ~ 0
VCC
Wire Wire Line
	6500 3900 6500 4200
Text GLabel 6500 3900 0    10   BiDi ~ 0
VEE
Wire Wire Line
	8300 3100 8300 3000
Text GLabel 8300 3100 0    10   BiDi ~ 0
VEE
Wire Wire Line
	9200 2200 9300 2200
Wire Wire Line
	9300 2200 9400 2200
Wire Wire Line
	8600 2200 9200 2200
Connection ~ 9300 2200
Connection ~ 9200 2200
Text GLabel 9200 2200 0    10   BiDi ~ 0
VEE
Wire Wire Line
	5800 4200 5900 4200
Wire Wire Line
	5900 4200 5900 4300
Text GLabel 5800 4200 0    10   BiDi ~ 0
VEE
Wire Wire Line
	8300 3700 8300 3600
Wire Wire Line
	8300 3600 8300 3500
Wire Wire Line
	8300 3600 8600 3600
Wire Wire Line
	8600 3600 8600 3700
Connection ~ 8300 3600
Text GLabel 8400 3600 2    70   BiDi ~ 0
REF_-4.096
Wire Wire Line
	4100 3500 4600 3500
Wire Wire Line
	4600 3500 4600 3700
Wire Wire Line
	4600 3700 4600 3800
Wire Wire Line
	4600 3800 4700 3800
Wire Wire Line
	4600 3700 4700 3700
Connection ~ 4600 3700
Wire Wire Line
	4100 3300 4600 3300
Wire Wire Line
	4600 3300 4600 3200
Wire Wire Line
	4600 3200 4700 3200
Wire Wire Line
	4600 3300 4700 3300
Connection ~ 4600 3300
$Comp
L module_tester-v02-eagle-import:78XXS IC8
U 1 1 59CA2EB8
P 5400 1400
AR Path="/59CA2EB8" Ref="IC8"  Part="1" 
AR Path="/61BE26B0/59CA2EB8" Ref="IC8"  Part="1" 
F 0 "IC8" H 5500 1100 59  0000 L BNN
F 1 "7805" H 5500 1000 59  0000 L BNN
F 2 "module_tester-v02:78XXS" H 5400 1400 50  0001 C CNN
F 3 "" H 5400 1400 50  0001 C CNN
	1    5400 1400
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:C-US025-025X050 C19
U 1 1 5D88D454
P 4900 1600
AR Path="/5D88D454" Ref="C19"  Part="1" 
AR Path="/61BE26B0/5D88D454" Ref="C19"  Part="1" 
F 0 "C19" H 4940 1625 59  0000 L BNN
F 1 "100n" H 4940 1435 59  0000 L BNN
F 2 "module_tester-v02:C025-025X050" H 4900 1600 50  0001 C CNN
F 3 "" H 4900 1600 50  0001 C CNN
	1    4900 1600
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:CPOL-USE2.5-7 C26
U 1 1 CE145288
P 6400 1600
AR Path="/CE145288" Ref="C26"  Part="1" 
AR Path="/61BE26B0/CE145288" Ref="C26"  Part="1" 
F 0 "C26" H 6440 1625 59  0000 L BNN
F 1 "100u" H 6440 1435 59  0000 L BNN
F 2 "module_tester-v02:E2,5-7" H 6400 1600 50  0001 C CNN
F 3 "" H 6400 1600 50  0001 C CNN
	1    6400 1600
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:LETTER_L #FRAME4
U 2 1 B998442F
P 7700 7200
AR Path="/B998442F" Ref="#FRAME4"  Part="2" 
AR Path="/61BE26B0/B998442F" Ref="#FRAME4"  Part="2" 
F 0 "#FRAME4" H 7700 7200 50  0001 C CNN
F 1 "LETTER_L" H 7700 7200 50  0001 C CNN
F 2 "" H 7700 7200 50  0001 C CNN
F 3 "" H 7700 7200 50  0001 C CNN
	2    7700 7200
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:FRAME_A_L #FRAME10
U 1 1 CC7DE888
P 900 7200
AR Path="/CC7DE888" Ref="#FRAME10"  Part="1" 
AR Path="/61BE26B0/CC7DE888" Ref="#FRAME10"  Part="1" 
F 0 "#FRAME10" H 900 7200 50  0001 C CNN
F 1 "FRAME_A_L" H 900 7200 50  0001 C CNN
F 2 "" H 900 7200 50  0001 C CNN
F 3 "" H 900 7200 50  0001 C CNN
	1    900  7200
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:FRAME_A_L #FRAME10
U 2 1 CC7DE884
P 7700 7200
AR Path="/CC7DE884" Ref="#FRAME10"  Part="2" 
AR Path="/61BE26B0/CC7DE884" Ref="#FRAME10"  Part="2" 
F 0 "#FRAME10" H 7700 7200 50  0001 C CNN
F 1 "FRAME_A_L" H 7700 7200 50  0001 C CNN
F 2 "" H 7700 7200 50  0001 C CNN
F 3 "" H 7700 7200 50  0001 C CNN
	2    7700 7200
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:+5V #P+23
U 1 1 3F3FBE72
P 6400 1100
AR Path="/3F3FBE72" Ref="#P+23"  Part="1" 
AR Path="/61BE26B0/3F3FBE72" Ref="#P+023"  Part="1" 
F 0 "#P+023" H 6400 1100 50  0001 C CNN
F 1 "+5V" V 6300 900 59  0000 L BNN
F 2 "" H 6400 1100 50  0001 C CNN
F 3 "" H 6400 1100 50  0001 C CNN
	1    6400 1100
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND45
U 1 1 9E0B3A99
P 5400 2100
AR Path="/9E0B3A99" Ref="#GND45"  Part="1" 
AR Path="/61BE26B0/9E0B3A99" Ref="#GND045"  Part="1" 
F 0 "#GND045" H 5400 2100 50  0001 C CNN
F 1 "GND" H 5300 2000 59  0000 L BNN
F 2 "" H 5400 2100 50  0001 C CNN
F 3 "" H 5400 2100 50  0001 C CNN
	1    5400 2100
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:CPOL-USE2.5-7 C27
U 1 1 CCF46D11
P 6500 3200
AR Path="/CCF46D11" Ref="C27"  Part="1" 
AR Path="/61BE26B0/CCF46D11" Ref="C27"  Part="1" 
F 0 "C27" H 6540 3225 59  0000 L BNN
F 1 "100u" H 6540 3035 59  0000 L BNN
F 2 "module_tester-v02:E2,5-7" H 6500 3200 50  0001 C CNN
F 3 "" H 6500 3200 50  0001 C CNN
	1    6500 3200
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:VCC #P+29
U 1 1 F4C2CAB7
P 6500 2800
AR Path="/F4C2CAB7" Ref="#P+29"  Part="1" 
AR Path="/61BE26B0/F4C2CAB7" Ref="#P+029"  Part="1" 
F 0 "#P+029" H 6500 2800 50  0001 C CNN
F 1 "VCC" H 6460 2940 59  0000 L BNN
F 2 "" H 6500 2800 50  0001 C CNN
F 3 "" H 6500 2800 50  0001 C CNN
	1    6500 2800
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:CPOL-USE2.5-7 C28
U 1 1 FFE6D357
P 6500 3700
AR Path="/FFE6D357" Ref="C28"  Part="1" 
AR Path="/61BE26B0/FFE6D357" Ref="C28"  Part="1" 
F 0 "C28" H 6540 3725 59  0000 L BNN
F 1 "100u" H 6540 3535 59  0000 L BNN
F 2 "module_tester-v02:E2,5-7" H 6500 3700 50  0001 C CNN
F 3 "" H 6500 3700 50  0001 C CNN
	1    6500 3700
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:VEE #SUPPLY3
U 1 1 D5692080
P 6500 4300
AR Path="/D5692080" Ref="#SUPPLY3"  Part="1" 
AR Path="/61BE26B0/D5692080" Ref="#SUPPLY03"  Part="1" 
F 0 "#SUPPLY03" H 6500 4300 50  0001 C CNN
F 1 "VEE" H 6425 4425 59  0000 L BNN
F 2 "" H 6500 4300 50  0001 C CNN
F 3 "" H 6500 4300 50  0001 C CNN
	1    6500 4300
	-1   0    0    1   
$EndComp
$Comp
L module_tester-v02-eagle-import:1N4004 D9
U 1 1 01F958EF
P 5400 1100
AR Path="/01F958EF" Ref="D9"  Part="1" 
AR Path="/61BE26B0/01F958EF" Ref="D9"  Part="1" 
F 0 "D9" H 5500 1119 59  0000 L BNN
F 1 "1N4004" H 5500 1009 59  0000 L BNN
F 2 "module_tester-v02:DO41-10" H 5400 1100 50  0001 C CNN
F 3 "" H 5400 1100 50  0001 C CNN
	1    5400 1100
	-1   0    0    1   
$EndComp
$Comp
L module_tester-v02-eagle-import:1N581 D8
U 1 1 AE697D1D
P 5900 1600
AR Path="/AE697D1D" Ref="D8"  Part="1" 
AR Path="/61BE26B0/AE697D1D" Ref="D8"  Part="1" 
F 0 "D8" H 5810 1675 59  0000 L BNN
F 1 "1N5819" H 5810 1465 59  0000 L BNN
F 2 "module_tester-v02:DO41-7.6" H 5900 1600 50  0001 C CNN
F 3 "" H 5900 1600 50  0001 C CNN
	1    5900 1600
	0    -1   -1   0   
$EndComp
$Comp
L module_tester-v02-eagle-import:LM4041LP IC1
U 1 1 1AF1C015
P 8300 3800
AR Path="/1AF1C015" Ref="IC1"  Part="1" 
AR Path="/61BE26B0/1AF1C015" Ref="IC1"  Part="1" 
F 0 "IC1" H 8410 3875 59  0000 L BNN
F 1 "LM4040 4.1V" H 8410 3765 59  0000 L BNN
F 2 "module_tester-v02:LP_O-PBCY-W3" H 8300 3800 50  0001 C CNN
F 3 "" H 8300 3800 50  0001 C CNN
	1    8300 3800
	-1   0    0    1   
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND82
U 1 1 91F00AFF
P 8300 4200
AR Path="/91F00AFF" Ref="#GND82"  Part="1" 
AR Path="/61BE26B0/91F00AFF" Ref="#GND082"  Part="1" 
F 0 "#GND082" H 8300 4200 50  0001 C CNN
F 1 "GND" H 8200 4100 59  0000 L BNN
F 2 "" H 8300 4200 50  0001 C CNN
F 3 "" H 8300 4200 50  0001 C CNN
	1    8300 4200
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:R-US_0204_7 R2
U 1 1 24183500
P 8300 3300
AR Path="/24183500" Ref="R2"  Part="1" 
AR Path="/61BE26B0/24183500" Ref="R2"  Part="1" 
F 0 "R2" H 8150 3359 59  0000 L BNN
F 1 "470" H 8150 3170 59  0000 L BNN
F 2 "module_tester-v02:0204_7" H 8300 3300 50  0001 C CNN
F 3 "" H 8300 3300 50  0001 C CNN
	1    8300 3300
	0    1    1    0   
$EndComp
$Comp
L module_tester-v02-eagle-import:VEE #SUPPLY10
U 1 1 AC3081B0
P 8300 2900
AR Path="/AC3081B0" Ref="#SUPPLY10"  Part="1" 
AR Path="/61BE26B0/AC3081B0" Ref="#SUPPLY010"  Part="1" 
F 0 "#SUPPLY010" H 8300 2900 50  0001 C CNN
F 1 "VEE" H 8225 3025 59  0000 L BNN
F 2 "" H 8300 2900 50  0001 C CNN
F 3 "" H 8300 2900 50  0001 C CNN
	1    8300 2900
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:C-US025-025X050 C3
U 1 1 0AE61553
P 8600 3800
AR Path="/0AE61553" Ref="C3"  Part="1" 
AR Path="/61BE26B0/0AE61553" Ref="C3"  Part="1" 
F 0 "C3" H 8640 3825 59  0000 L BNN
F 1 "100n" H 8640 3635 59  0000 L BNN
F 2 "module_tester-v02:C025-025X050" H 8600 3800 50  0001 C CNN
F 3 "" H 8600 3800 50  0001 C CNN
	1    8600 3800
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND85
U 1 1 247FD0CC
P 8600 4200
AR Path="/247FD0CC" Ref="#GND85"  Part="1" 
AR Path="/61BE26B0/247FD0CC" Ref="#GND085"  Part="1" 
F 0 "#GND085" H 8600 4200 50  0001 C CNN
F 1 "GND" H 8500 4100 59  0000 L BNN
F 2 "" H 8600 4200 50  0001 C CNN
F 3 "" H 8600 4200 50  0001 C CNN
	1    8600 4200
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:M08X2 J7
U 1 1 1B1E17EB
P 8900 1900
AR Path="/1B1E17EB" Ref="J7"  Part="1" 
AR Path="/61BE26B0/1B1E17EB" Ref="J7"  Part="1" 
F 0 "J7" H 8800 2450 59  0000 L BNN
F 1 "M08X2" H 8800 1400 59  0000 L BNN
F 2 "module_tester-v02:2X8" H 8900 1900 50  0001 C CNN
F 3 "" H 8900 1900 50  0001 C CNN
	1    8900 1900
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND36
U 1 1 397F6EFB
P 9800 2100
AR Path="/397F6EFB" Ref="#GND36"  Part="1" 
AR Path="/61BE26B0/397F6EFB" Ref="#GND036"  Part="1" 
F 0 "#GND036" H 9800 2100 50  0001 C CNN
F 1 "GND" H 9700 2000 59  0000 L BNN
F 2 "" H 9800 2100 50  0001 C CNN
F 3 "" H 9800 2100 50  0001 C CNN
	1    9800 2100
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:VEE #SUPPLY4
U 1 1 6DF68C43
P 9400 2300
AR Path="/6DF68C43" Ref="#SUPPLY4"  Part="1" 
AR Path="/61BE26B0/6DF68C43" Ref="#SUPPLY04"  Part="1" 
F 0 "#SUPPLY04" H 9400 2300 50  0001 C CNN
F 1 "VEE" H 9325 2425 59  0000 L BNN
F 2 "" H 9400 2300 50  0001 C CNN
F 3 "" H 9400 2300 50  0001 C CNN
	1    9400 2300
	-1   0    0    1   
$EndComp
$Comp
L module_tester-v02-eagle-import:VCC #P+14
U 1 1 A9B0ECED
P 9700 1800
AR Path="/A9B0ECED" Ref="#P+14"  Part="1" 
AR Path="/61BE26B0/A9B0ECED" Ref="#P+014"  Part="1" 
F 0 "#P+014" H 9700 1800 50  0001 C CNN
F 1 "VCC" H 9660 1940 59  0000 L BNN
F 2 "" H 9700 1800 50  0001 C CNN
F 3 "" H 9700 1800 50  0001 C CNN
	1    9700 1800
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:+5V #P+15
U 1 1 E802E752
P 9500 1500
AR Path="/E802E752" Ref="#P+15"  Part="1" 
AR Path="/61BE26B0/E802E752" Ref="#P+015"  Part="1" 
F 0 "#P+015" H 9500 1500 50  0001 C CNN
F 1 "+5V" V 9400 1300 59  0000 L BNN
F 2 "" H 9500 1500 50  0001 C CNN
F 3 "" H 9500 1500 50  0001 C CNN
	1    9500 1500
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:JACK-PLUG2 J9
U 1 1 5EBF55DB
P 3900 3400
AR Path="/5EBF55DB" Ref="J9"  Part="1" 
AR Path="/61BE26B0/5EBF55DB" Ref="J9"  Part="1" 
F 0 "J9" H 3600 3600 59  0000 L BNN
F 1 "JACK-PLUG2" H 3600 3200 59  0000 L BNN
F 2 "module_tester-v02:SPC4078_BIG_HOLES" H 3900 3400 50  0001 C CNN
F 3 "" H 3900 3400 50  0001 C CNN
	1    3900 3400
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:M02PTH JP3
U 1 1 ACD8E5B6
P 5000 3200
AR Path="/ACD8E5B6" Ref="JP3"  Part="1" 
AR Path="/61BE26B0/ACD8E5B6" Ref="JP3"  Part="1" 
F 0 "JP3" H 4900 3430 59  0000 L BNN
F 1 "M02PTH" H 4900 3000 59  0000 L BNN
F 2 "module_tester-v02:1X02" H 5000 3200 50  0001 C CNN
F 3 "" H 5000 3200 50  0001 C CNN
	1    5000 3200
	-1   0    0    1   
$EndComp
$Comp
L module_tester-v02-eagle-import:M02PTH JP4
U 1 1 47274FB3
P 5000 3700
AR Path="/47274FB3" Ref="JP4"  Part="1" 
AR Path="/61BE26B0/47274FB3" Ref="JP4"  Part="1" 
F 0 "JP4" H 4900 3930 59  0000 L BNN
F 1 "M02PTH" H 4900 3500 59  0000 L BNN
F 2 "module_tester-v02:1X02" H 5000 3700 50  0001 C CNN
F 3 "" H 5000 3700 50  0001 C CNN
	1    5000 3700
	-1   0    0    1   
$EndComp
$Comp
L module_tester-v02-eagle-import:M01PTH JP9
U 1 1 E957C24B
P 5500 2700
AR Path="/E957C24B" Ref="JP9"  Part="1" 
AR Path="/61BE26B0/E957C24B" Ref="JP9"  Part="1" 
F 0 "JP9" H 5400 2830 59  0000 L BNN
F 1 "M01PTH" H 5400 2500 59  0000 L BNN
F 2 "module_tester-v02:1X01" H 5500 2700 50  0001 C CNN
F 3 "" H 5500 2700 50  0001 C CNN
	1    5500 2700
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:M01PTH JP6
U 1 1 7F37E81D
P 5500 3200
AR Path="/7F37E81D" Ref="JP6"  Part="1" 
AR Path="/61BE26B0/7F37E81D" Ref="JP6"  Part="1" 
F 0 "JP6" H 5400 3330 59  0000 L BNN
F 1 "M01PTH" H 5400 3000 59  0000 L BNN
F 2 "module_tester-v02:1X01" H 5500 3200 50  0001 C CNN
F 3 "" H 5500 3200 50  0001 C CNN
	1    5500 3200
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:M01PTH JP7
U 1 1 600E0B9C
P 5500 3700
AR Path="/600E0B9C" Ref="JP7"  Part="1" 
AR Path="/61BE26B0/600E0B9C" Ref="JP7"  Part="1" 
F 0 "JP7" H 5400 3830 59  0000 L BNN
F 1 "M01PTH" H 5400 3500 59  0000 L BNN
F 2 "module_tester-v02:1X01" H 5500 3700 50  0001 C CNN
F 3 "" H 5500 3700 50  0001 C CNN
	1    5500 3700
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:M01PTH JP8
U 1 1 A6258458
P 5500 4200
AR Path="/A6258458" Ref="JP8"  Part="1" 
AR Path="/61BE26B0/A6258458" Ref="JP8"  Part="1" 
F 0 "JP8" H 5400 4330 59  0000 L BNN
F 1 "M01PTH" H 5400 4000 59  0000 L BNN
F 2 "module_tester-v02:1X01" H 5500 4200 50  0001 C CNN
F 3 "" H 5500 4200 50  0001 C CNN
	1    5500 4200
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:GND #GND38
U 1 1 9C9B1271
P 6200 3700
AR Path="/9C9B1271" Ref="#GND38"  Part="1" 
AR Path="/61BE26B0/9C9B1271" Ref="#GND038"  Part="1" 
F 0 "#GND038" H 6200 3700 50  0001 C CNN
F 1 "GND" H 6100 3600 59  0000 L BNN
F 2 "" H 6200 3700 50  0001 C CNN
F 3 "" H 6200 3700 50  0001 C CNN
	1    6200 3700
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:VCC #P+16
U 1 1 483164F1
P 5900 2600
AR Path="/483164F1" Ref="#P+16"  Part="1" 
AR Path="/61BE26B0/483164F1" Ref="#P+016"  Part="1" 
F 0 "#P+016" H 5900 2600 50  0001 C CNN
F 1 "VCC" H 5860 2740 59  0000 L BNN
F 2 "" H 5900 2600 50  0001 C CNN
F 3 "" H 5900 2600 50  0001 C CNN
	1    5900 2600
	1    0    0    -1  
$EndComp
$Comp
L module_tester-v02-eagle-import:VEE #SUPPLY1
U 1 1 E46721A6
P 5900 4400
AR Path="/E46721A6" Ref="#SUPPLY1"  Part="1" 
AR Path="/61BE26B0/E46721A6" Ref="#SUPPLY01"  Part="1" 
F 0 "#SUPPLY01" H 5900 4400 50  0001 C CNN
F 1 "VEE" H 5825 4525 59  0000 L BNN
F 2 "" H 5900 4400 50  0001 C CNN
F 3 "" H 5900 4400 50  0001 C CNN
	1    5900 4400
	-1   0    0    1   
$EndComp
$Comp
L module_tester-v02-eagle-import:VCC #P+17
U 1 1 E83F5DEA
P 4700 1300
AR Path="/E83F5DEA" Ref="#P+17"  Part="1" 
AR Path="/61BE26B0/E83F5DEA" Ref="#P+017"  Part="1" 
F 0 "#P+017" H 4700 1300 50  0001 C CNN
F 1 "VCC" H 4660 1440 59  0000 L BNN
F 2 "" H 4700 1300 50  0001 C CNN
F 3 "" H 4700 1300 50  0001 C CNN
	1    4700 1300
	1    0    0    -1  
$EndComp
Text Notes 7800 6000 0    127  ~ 25
Power supply
Text Notes 7800 6200 0    85   ~ 0
Émilie Gillet, 2012. cc-by-sa
Text Notes 3600 3700 0    59   ~ 0
15V DC
$EndSCHEMATC
