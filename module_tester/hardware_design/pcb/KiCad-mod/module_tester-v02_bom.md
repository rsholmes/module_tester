# module_tester-v02.kicad_sch BOM

Tue 01 Mar 2022 07:36:07 PM EST

Generated from schematic by Eeschema 6.0.2-378541a8eb~116~ubuntu20.04.1

**Component Count:** 107

| Refs | Qty | Component | Description | Vendor | SKU |
| ----- | --- | ---- | ----------- | ---- | ---- |
| C1, C7, C13, C14 | 4 | 18p | Ceramic capacitor | Digi-Key | BC1004CT-ND |
| C2, C3, C4, C5, C11, C15, C16, C17, C18, C19, C20, C21, C23, C25 | 14 | 100n | Ceramic capacitor | Tayda | A-553 |
| C6 | 1 | 4.7u | Electrolytic capacitor | Tayda | A-4555 |
| C8, C22 | 2 | 1n | Ceramic capacitor | Tayda | A-965 |
| C9, C27, C28, C26 | 3 | 100u | Electrolytic capacitor | Tayda | A-4538 |
| C24 | 1 | 4.7u NP | Non polarized electrolytic capacitor | Tayda | A-4630 |
| D1 | 1 | 1N4148 | Switching diode | Tayda | A-157 |
| D8 | 1 | 1N5819 | Schottky diode | Tayda | A-484 |
| D9 | 1 | 1N4004 | Rectifier diode | Tayda | A-156 |
| IC1 | 1 | LM4040 4.1V | Voltage reference | Digi-Key | LM4040DIZ-4.1/NOPB-ND |
| IC2 | 1 | TL074P | Quad op amp | Tayda | A-1138 |
| IC3 | 1 | MCP4822 | Dual DAC | Digi-Key | MCP4822-E/P-ND |
| IC4 | 1 | ATMEGA644p | Microcontroller | Digi-Key | ATMEGA644PV-10PU-ND |
| IC5 | 1 | 74595N | Shift register | Tayda | A-251 |
| IC6 | 1 | 74HC165N | Shift register | Tayda | A-639 |
| IC7 | 1 | MCP6002 | Dual op amp | Tayda | MCP6002-I/P-ND |
| IC8 | 1 | 7805 | Voltage regulator | Tayda | A-179 |
| J1, J2, J3, J4, J5, J10 | 6 | PJ301_THONKICONN6 | 3.5 mm audio jack, vertical  | SynthCube |  |
| J7 | 1 | M08X2 | 2x8 IDC header | Tayda | A-2958 |
| J8 | 1 | MONOSWSLIM | 1/4" audio jack, horizontal | Tayda | A-1122 |
| J9 | 1 | JACK-PLUG2 | Power jack | Digi-Key | CP-202A-ND |
| JP3–9 | 1 | M02PTH | 15 V to ±12 V power converter | Digi-Key | 1866-5074-ND |
| LED1, LED2, LED3, LED4, LED5, LED6 | 6 | LED3MM | 3mm LED | Tayda | A-261 |
| OK1 | 1 | 6N137 | Optoisolator | Tayda | A-871 |
| Q1 | 1 | 2N3904 | NPN transistor | Tayda | A-111 |
| Q2 | 1 | 20MHz | Crystal | Tayda | A-236 |
| R1, R2, R34, R35 | 4 | 470 | 1/4 W metal film resistor | Tayda | A-2247 |
| R3, R19, R20, R21, R22, R23, R24, R25, R27 | 9 | 220 | 1/4 W metal film resistor | Tayda | A-2246 |
| R4, R28 | 2 | 33k | 1/4 W metal film resistor | Tayda | A-2290 |
| R5, R6, R17, R26 | 4 | 10k | 1/4 W metal film resistor | Tayda | A-2203 |
| R7, R18, R29, R31, R33 | 5 | 100k | 1/4 W metal film resistor | Tayda | A-2248 |
| R8 | 1 | 5k | Multi turn trimmer | Digi-Key | 3339P-502LF-ND |
| R9 | 1 | 20k | Multi turn trimmer | Tayda | A-592 |
| R10 | 1 | 82k | 1/4 W metal film resistor | Tayda | A-2338 |
| R11 | 1 | 62k | 1/4 W metal film resistor | Tayda | A-2628 |
| R12, R32 | 2 | 47k | 1/4 W metal film resistor | Tayda | A-2279 |
| R13 | 1 | 5k | Multi turn trimmer | Tayda | A-597 |
| R14 | 1 | 0 | 0 ohm resistor or wire jumper* |  |  |
| R15, R16 | 2 | 150k | 1/4 W metal film resistor | Tayda | A-2210 |
| R30 | 1 | 1M | 1/4 W metal film resistor | Tayda | A-2277 |
| RN1 | 1 | G06R | Bussed resistor network | Digi-Key | 4607X-101-103LF-ND |
| S1, S2, S3, S4, S5, S6 | 6 | 40-XX | SPST tactile push switch | Digi-Key | EG1821-ND |
| SW1 | 1 | Encoder | Rotary encoder | Digi-Key | PEC12R-4217F-S0024-ND |
| U1 | 1 | ISP | 2x4 pin header | Tayda | A-198 |
| U$3 | 1 | LCD_MODULE2X16 | 2x16 LCD module | Tayda | A-1748 |
| X1 | 1 | MIDI | 5 pin circular DIN connector, horizontal | Digi-Key | 2092-KCDX-5S-N-ND |
| | 3 | | 8 pin DIP socket | Tayda | A-001 |
| | 1 | | 14 pin DIP socket | Tayda | A-004 |
| | 2 | | 16 pin DIP socket | Tayda | A-003 |
| | 1 | | 40 pin DIP socket | Tayda | A-292 |
| | 1 | | D shaft knob      | Tayda | A-6056 |
| | 1 | | Heat sink (optional)** | Tayda | A-5093 |
| | | | Thermal paste (optional)** | Tayda |  A-759 |
| | | | Enclosure and mounting hardware | | |
| | 1 | | 15V DC 1 A power supply | | |

\* Or ~100R resistor depending on LCD module (BOM LCD does not need external resistor)

\*\* Almost certainly not needed
