# Module tester notes

This version has been modified to make the PCB larger, to fit in an existing box.

Additionally, I switched the footprints for the MIDI and 1/4" phone jacks to match parts I had in my stash.

## Notes from [https://www.amazingsynth.com/module-tester-pcb/](https://www.amazingsynth.com/module-tester-pcb/) 

This dc-dc converter has been tested by a few people (thanks guys), it's about 60% cheaper than the original one specced in the BOM, it has about 25% more ripple, it's 100mv/v rather than 75mv (per volt) 

[https://www.mouser.com/ProductDetail/MEAN-WELL/DCWN06A-12?qs=sGAEpiMZZMvGsmoEFRKS8Koqt8Pjkl39NlqtsO2abGES3WFRVPS%2Fng==](https://www.mouser.com/ProductDetail/MEAN-WELL/DCWN06A-12?qs=sGAEpiMZZMvGsmoEFRKS8Koqt8Pjkl39NlqtsO2abGES3WFRVPS%2Fng==)

This is a Mouser Cart BOM for easy ordering of most parts and for costing your builds. 

[Module Tester V2 BOM](https://www.mouser.com/ProjectManager/ProjectDetail.aspx?AccessID=f7a6ea49cf)

